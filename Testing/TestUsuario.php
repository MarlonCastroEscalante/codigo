<?php

require_once('../Datos/DatosUsuario.php');

class TestUsuario extends PHPUnit_Framework_TestCase{

	public function setUp(){ }
	public function tearDown(){}

	public function testInsercionDebeDevolverTrue(){
		$this->assertTrue(insertarUsuario('402280426','Alvaro Gonzalez','ventanilla','varogonz95'));
	}

	/**
	* @depends testInsercionDebeDevolverTrue
	*/
	public function testConsultaNoDebeSerVacio(){
		$this->assertNotEmpty(mostrarUsuarios());
	}

	/**
	* @depends testInsercionDebeDevolverTrue
	*/
	public function testModificarDebeDevolverTrue(){
		$usuarios=mostrarUsuarios();
		$u=$usuarios[count($usuarios)-1];
		$this->assertTrue(modificarUsuario($u->getCedula(),$u->getNombre(),$u->getRol(),$u->getClave()));
	}

	/**
	* @depends testInsercionDebeDevolverTrue
	*/
	public function testEliminarDebeDevolverTrue(){
		$usuarios=mostrarUsuarios();
		$ced=$usuarios[count($usuarios)-1]->getCedula();
		$this->assertTrue(eliminarUsuario($ced));
	}
}
