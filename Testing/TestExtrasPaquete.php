<?php
require_once('../Datos/DataExtrasPaquetes.php');

class TestExtrasPaquete extends PHPUnit_Framework_TestCase{

    public function setUp(){ }
    public function tearDown(){ }


    public function testCategoriaDebeAcertarSiLaInsercionNoFalla(){
        $this->assertTrue(insertarCategoria('Prueba insertar categoria'));
    }

    public function testCategoriaDebeAcertarSiLaInsercionIdNoFalla(){
        $this->assertTrue(insertarCategoriaId('Prueba insertar categoria Id')!==false);
    }

    /**
    * @depends testCategoriaDebeAcertarSiLaInsercionNoFalla
    */
    public function testCategoriaDebeAcertarSiLaConsultaNoFalla(){
        $this->assertTrue(!listarCategorias());
    }

    /**
     * @depends testCategoriaDebeAcertarSiLaInsercionIdNoFalla
     */
    public function testCategoriaDebeAcertarSiLaModificacionNoFalla(){
        $id=insertarCategoriaId('Prueba modificar categoria');
        $this->assertTrue(!modificarCategoria($id,'Prueba modificar categoria (modificada)'));
    }

    public function testDimensionDebeAcertarSiLaInsercionNoFalla(){
        $this->assertTrue(insertarDimension('Prueba insertar dimension',1,2,3));
    }

    public function testDimensionDebeAcertarSiLaInsercionIdNoFalla(){
        $this->assertTrue(insertarCategoriaId('Prueba insertar dimension Id',1,2,3)!==false);
    }

    /**
    * @depends testDimensionDebeAcertarSiLaInsercionNoFalla
    */
    public function testDimensionDebeAcertarSiLaConsultaNoFalla(){
        $this->assertTrue(!listarDimensiones());
    }

    /**
     * @depends testDimensionDebeAcertarSiLaInsercionIdNoFalla
     */
    public function testDimensionDebeAcertarSiLaModificacionNoFalla(){
        $id=insertarCategoriaId('Prueba modificar dimension',3,2,1);
        $this->assertTrue(!modificarCategoria($id,'Prueba modificar dimension (modificada)',1,2,3));
    }
}
