<?php
require_once('../Datos/DatosBus.php');
class TestBus extends PHPUnit_Framework_TestCase{

	public function setUp(){ }
	public function tearDown(){ }

	public function testInsercionDebeSerTrue(){
		$this->assertTrue(insertarBus('MATRICULA123456',60,'bueno','turismo'));
	}

	/*
	* @depends testInsercionDebeSerTrue
	*/
	public function testConsultaNoDebeSerVacia(){
		$this->assertNotEmpty(buscarBus(1));
	}
}
