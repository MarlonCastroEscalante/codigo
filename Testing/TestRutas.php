<?php
require_once('../Datos/datosRuta.php');
class TestRutas extends PHPUnit_Framework_TestCase{

    public $rutas=[];
    public $ruta=null;

	public function setUp(){
        $this->rutas=json_decode(testGetRutas());
        $this->ruta=$this->rutas[count($this->rutas)-1];
    }
	public function tearDown(){ }

    public function testInsercionDebeSerTrue(){
        $this->assertTrue(insertarRuta('Prueba lugar salida','Prueba lugar llegada','Prueba comentario',123));
    }

    /**
    * @depends testInsercionDebeSerTrue
    */
    public function testModificarDebeSerTrue(){
        $this->assertTrue(modificarRuta($this->ruta->id,'Prueba lugar salida editado','Prueba lugar llegada editado','Prueba comentario editado',321));
    }

	public function testEliminarDebeSerTrue(){
		$this->assertTrue(eliminarRuta($this->ruta->id));
	}

}
