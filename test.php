<?php include './Datos/datosRuta.php';?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>

    <link rel="stylesheet" href="./Vistas/css/global.css">
    <link rel="stylesheet" href="./Vistas/css/navbar.css">
    <link rel="stylesheet" href="./Vistas/css/glyphicons.css">
    <link rel="stylesheet" href="./Vistas/css/rutas.css">
    <link rel="stylesheet" href="./Vistas/css/rutas_admin.css">
    <link rel="stylesheet" href="./Vistas/js/animatedModal/animate.min.css">
    <link rel="stylesheet" href="./Vistas/js/animatedModal/animatedModal.css">
  </head>
  <body>

      <?php
        //include './Vistas/partials/navbar2.php';

        $MAX_ROWS=5;
        $MAX_PAGINATION_SIZE=5;
        //$CURR_PAGE_INDEX=intval($_GET['page']);

        $rutas=testGetRutas();
      ?>

      <section class="row">

          <header><h1>Administrar rutas y horarios.</h1></header>

          <nav class="lg-6 push-lg-3">
              <div class="controls">
                  <button type="button" onclick="modal.show()" class="btn btn-primary">Agregar nueva ruta</button>
              </div>
              <form id="busqueda" onsubmit="return false">
                  <div id="input-wrapper" class="form-group">
                      <button id="fdt" type="button" class="input-button-dropdown" data-target="filter-dropdown" data-toggle="dropdown"><span class="glyphicon glyphicon-road"></span><span style="font-size:x-small" class="glyphicon glyphicon-triangle-bottom"></span></button>
                      <ul id="filter-dropdown" class="dropdown">
                          <li class="dropdown-header">Filtros<hr class="separator"></li>
                          <li class="dropdown-item" onclick="setFilter(0,'#fdt')"><span class="glyphicon glyphicon-road"></span> <span>Rutas</span></li>
                          <li class="dropdown-item" onclick="setFilter(1,'#fdt')"><span class="glyphicon glyphicon-map-marker"></span> <span>Paradas</span></li>
                      </ul>
                      <input id="buscar-ruta" class="form-control" type="text" name="search" placeholder="Búsqueda..." onkeyup="autoCompleteSearch(this)" autocomplete="off">
                      <span class="glyphicon glyphicon-search right"></span>
                      <ul id="iwr"></ul>
                  </div>
              </form>
          </nav>

          <section class="lg-8 md-8 sm-12 xs-12 push-lg-2">
              <ul class="routes-list" >
                  <?php
                        for ($i=0,$len=count($rutas); $i < $MAX_ROWS && $i < $len; $i++) {
                        $data=testGetAllRouteData($rutas[$i]['id'],1,false);
                    ?>
                  <li class="route-item" data-ruta-salida="<?php echo $rutas[$i]['salida']; ?>" data-ruta-destino="<?php echo $rutas[$i]['destino']; ?>" data-ruta-id="<?php echo $rutas[$i]['id']; ?>">
                      <div class="route-title">
                          <button type="button" class="glyphicon glyphicon-menu-right left" onclick="collapseContent(this,<?php echo $i?>)"></button>
                          <h3 class="left"><?php echo $rutas[$i]['salida']; ?> - <?php echo $rutas[$i]['destino']; ?></h3>
                      </div>
                      <div class="route-content">
                          <button type="button" class="btn-map-opts right"><span class="glyphicon-option-horizontal glyphicon"></span></button>
                          <div id="map-id-<?php echo $i;?>" class="map-container"></div>
                          <div class="stops lg-5">
                              <h3><span onclick="toggleCollapse(this,'selector')" class="glyphicon glyphicon-menu-down" style="font-size:small;color:#999"></span><span>Paradas</span><hr class="separator"></h3>
                              <div class="stops-list"></div>
                          </div>
                          <div class="schedule lg-5 push-lg-1">
                              <h3><span onclick="toggleCollapse(this,'#schedule-list')" class="glyphicon glyphicon-menu-down" style="font-size:small;color:#999"></span><span>Horarios</span><hr class="separator"></h3>
                              <div class="schedule-list" style="overflow:hidden"></div>
                          </div>
                      </div>
                  </li>
                  <?php } ?>
              </ul>
          </section>
      </section>
  </body>

  <script src="./Vistas/js/jquery.google.js" charset="utf-8"></script>
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDtBqquuE1ttZMBTbCl8bnw0GEc0tKGrro"></script>
  <script src="./Vistas/js/animatedModal/animatedModal.js" charset="utf-8"></script>
  <script src="./Vistas/js/leanModal/jquery.leanModal.min.js" charset="utf-8"></script>
  <script src="./Vistas/js/rutas_admin.js" charset="utf-8"></script>
</html>
