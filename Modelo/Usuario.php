<?php
	class Usuario{

		private $cedula;
		private $nombre;
		private $rol;
		private $clave;

		public function __construct(){}

		public function setCedula($cedula){
			 $this->cedula = $cedula;
		}

		public function setNombre($nombre){
			 $this->nombre = $nombre;
		}

		public function setRol($rol){
			$this->rol = $rol;
		}

		public function setClave($clave){
			$this->clave = $clave;
		}

		public function getCedula(){
			return $this->cedula;
		}

		public function getNombre(){
			return $this->nombre;
		}

		public function getRol(){
			return $this->rol;
		}

		public function getClave(){
			return $this->clave;
		}

	}