<?php
	class Ruta{

		private $codigo;
		private $codigoRuta;
		private $costo;
		private $estado;
		private $codigoSucursal;
		private $fecha;

		public function __construct(){}

		public function setCodigo($codigo){
			 $this->codigo = $codigo;
		}

		public function setCodigoRuta($codigoRuta){
			 $this->codigoRuta = $codigoRuta;
		}

		public function setCosto($costo){
			 $this->costo = $costo;
		}

		public function setEstado($estado){
			 $this->estado = $estado;
		}

		public function setCodigoSucursal($codigoSucursal){
			 $this->codigoSucursal = $codigoSucursal;
		}

		public function setFecha($fecha){
			 $this->fecha = $fecha;
		}

		public function getCodigo(){
			return $this->codigo;
		}

		public function getCodigoRuta(){
			return $this->codigoRuta;
		}

		public function getCosto(){
			return $this->costo;
		}

		public function getEstado(){
			return $this->estado;
		}

		public function getCodigoSucursal(){
			return $this->codigoSucursal;
		}

		public function getFecha(){
			return $this->fecha;
		}
	}

?>