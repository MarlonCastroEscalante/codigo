<?php
	class Encomienda{

		private $numeroGuia;
		private $fecha;
		private $precio;
		private $comentario;
		private $destino;
		private $origen;
		private $estado;
		private $paquetes;

		public function __construct(){
			$this->paquetes = array();
		}

		public function setNumeroGuia($numeroGuia){
			 $this->numeroGuia = $numeroGuia;
		}

		public function setFecha($fecha){
			 $this->fecha = $fecha;
		}

		public function setPrecio($precio){
			$this->precio = $precio;
		}

		public function setEstado($estado){
			$this->estado = $estado;
		}

		public function setPaquetes($paquetes){
			$this->paquetes = $paquetes;
		}

		public function setComentario($comentario){
			 $this->comentario = $comentario;
		}

		public function setOrigen($origen){
			$this->origen = $origen;
		}

		public function setDestino($destino){
			$this->destino = $destino;
		}

		public function getNumeroGuia(){
			return $this->numeroGuia;
		}

		public function getFecha(){
			return $this->fecha;
		}

		public function getPrecio(){
			return $this->precio;
		}

		public function getPaquetes(){
			return $this->paquetes;
		}

		public function getComentario(){
			return $this->comentario;
		}

		public function getOrigen(){
			return $this->origen;
		}

		public function getDestino(){
			return $this->destino;
		}

		public function getEstado(){
			return $this->estado;
		}

	}
