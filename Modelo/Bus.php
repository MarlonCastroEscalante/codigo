<?php

class Bus{

		private $matricula;
		private $capacidad;
		private $estado;
		private $tipo;
		private $imagen;

		public function __construct(){}

		public function setMatricula($matricula){
			 $this->matricula = $matricula;
		}

		public function setEstado($estado){
			 $this->estado = $estado;
		}

		public function setTipo($tipo){
			$this->tipo = $tipo;
		}

		public function setCapacidad($capacidad){
			$this->capacidad = $capacidad;
		}

		public function setImagen($imagen){
			$this->imagen = $imagen;
		}
		public function getMatricula(){
			return $this->matricula;
		}

		public function getEstado(){
			return $this->estado;
		}

		public function getTipo(){
			return $this->tipo;
		}

		public function getCapacidad(){
			return $this->capacidad;
		}

		public function getImagen(){
			return $this->imagen;
		}

	}
?>