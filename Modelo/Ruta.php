<?php
	class Ruta{

		private $codigo;
		private $origen;
		private $destino;
		private $tiempoEstimado;
		private $paradas;

		public function __construct(){
			$this->paradas = array();
		}

		public function setCodigo($codigo){
			 $this->codigo = $codigo;
		}

		public function setOrigen($origen){
			$this->origen = $origen;
		}

		public function setDestino($destino){
			$this->destino = $destino;
		}

		public function setTiempoEstimado($tiempo){
			$this->tiempoEstimado = $tiempo;
		}

		public function setParadas($paradas){
			$this->paradas = $paradas;
		}

		public function getCodigo(){
			return $this->codigo;
		}

		public function getOrigen(){
			return $this->origen;
		}

		public function getDestino(){
			return $this->destino;
		}

		public function getTiempoEstimado(){
			return $this->tiempoEstimado;
		}

		public function getParadas(){
			return $this->paradas;
		}

	}