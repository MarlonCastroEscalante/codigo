<?php
	class Paquete{

		private $peso;
		private $categoria;
		private $contenido;

		public function __construct(){}

		public function setPeso($peso){
			 $this->peso = $peso;
		}

		public function setCategoria($categoria){
			 $this->categoria = $categoria;
		}

		public function setContenido($contenido){
			$this->contenido = $contenido;
		}

		public function getPeso(){
			return $this->peso;
		}

		public function getCategoria(){
			return $this->categoria;
		}

		public function getContendio(){
			return $this->contenido;
		}

	}