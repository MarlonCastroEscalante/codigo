<?php
	class Parada{

		private $codigo;
		private $nombre;
		private $precio;

		public function __construct(){}

		public function setCodigo($codigo){
			 $this->codigo = $codigo;
		}

		public function setNombre($nombre){
			 $this->nombre = $nombre;
		}

		public function setPrecio($precio){
			$this->precio = $precio;
		}

		public function getCodigo(){
			return $this->codigo;
		}

		public function getNombre(){
			return $this->nombre;
		}

		public function getPrecio(){
			return $this->precio;
		}

	}