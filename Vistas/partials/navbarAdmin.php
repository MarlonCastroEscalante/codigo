<div class="banner">
  <nav class="navbar">
    <div class="navbar-item lg-2 md-2 sm-3 xs-8"><a class="brand" href="#">Aplicacion</a></div>
    <div class="navbar-item right"><button ui-action="collapse" ui-target="menu-collapse" class="glyphicon glyphicon-menu-hamburger btn collapse-xs" type="button" id="collapse-btn"></button></div>

        <div>
             <ul class="navbar-item-group">
              <li class="navbar-item-admin"><a href="#home">Usuarios</a></li>
              <li class="navbar-item-admin"><a href="encomiendas.php">Encomiendas</a></li>
              <li class="navbar-item-admin"><a href="#news">Rutas</a></li>
              <li class="dropdown-admin navbar-item-admin">
                <a href="javascript:void(0)" class="dropbtn">Buses</a>
                <div class="dropdown-content">
                  <a href="#">Gestión de flotilla</a>
                  <a href="./alquilerBusesAdmi.php">Alquiler</a>
                </div>
              </li>
            </ul>  
            <div class="navbar-item-group right">
                <a class="navbar-item navbar-item-link" style="font-size:large" href="#"><?php echo "{$_SESSION['user']['nombre']}"; ?></a>
                <a class="navbar-item navbar-item-link" href="<?php echo ($_SERVER['PHP_SELF']==='index.php')?'.':'..' ?>/Controladoras/UsuarioController.php?handler=logout">Salir</a>
            </div>    
        </div>

            
        </nav>
</div> 

