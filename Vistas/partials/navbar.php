<nav class="navbar">
    <div class="navbar-item"><span class="brand">Aplicacion</span></div>
    <a class="navbar-item" href="../index.php">Inicio</a>

    <?php if(isset($_SESSION['user'])){ ?>
    <div class="navbar-item-group right">
        <?php if(isset($_SESSION['user']['sucursal'])){ ?>
        <a class="navbar-item" style="font-size:small" href="#"><?php echo $_SESSION['user']['sucursal']; ?></a>
        <?php } ?>
        <a class="navbar-item" style="font-size:small" href="#"><?php echo $_SESSION['user']['nombre']; ?></a>
        <a class="navbar-item" href="../Controladoras/UsuarioController.php?handler=logout">Salir</a>
    </div>
    <?php } else{ ?>
        <div class="navbar-item-group right">
            <form class="login left" action="/Datos/DatosUsuario.php" method="post">
                <input type="hidden" name="accion" value="1">
                <div class="form-inline">
                    <input class="form-control" type="text" name="ced" placeholder="No. Identificación" required>
                </div>
                <div class="form-inline">
                    <input class="form-control" type="password" name="passw" placeholder="Contraseña" required>
                </div>
                <div class="form-inline">
                    <button class="btn btn-success" type="submit">Iniciar sesión</button>
                </div>
            </form>
            <a class="navbar-item left" href="#">Registrarse</a>
        </div>
    <?php } ?>
</nav>
