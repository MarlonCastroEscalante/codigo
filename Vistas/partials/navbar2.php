<div class="banner">

<nav class="navbar">
    <div class="navbar-item lg-2 md-2 sm-3 xs-8"><a class="brand" href="<?php echo ($_SERVER['PHP_SELF']==='index.php')?'.':'..' ?>/index.php">Inicio</a></div>

    <?php if(!isset($_SESSION['user'])){ ?>
        <div class="navbar-item right xs-12"><a onclick="$('#login-collapse').slideToggle(300)"><img src="<?php echo ($_SERVER['PHP_SELF']==='index.php')?'.':'..' ?>/Vistas/img/login.svg" width="20" height="20" style="vertical-align:middle" alt=""> Acceder al sistema</a></div>
            <div class="login xs-12" id="login-collapse">
                <form action="<?php echo ($_SERVER['PHP_SELF']==='index.php')?'.':'..' ?>/Datos/DatosUsuario.php" method="post">
                    <input type="hidden" name="accion" value="1">
                    <div class="form-group">
                        <input class="form-control" type="text" name="ced" placeholder="No. Identificación" required>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" name="passw" placeholder="Contraseña" required>
                    </div>
                    <div id="login-validations">
                        <p><span class="glyphicon glyphicon-remove"></span><span class="msg"></span></p>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit">Iniciar sesión</button>
                    </div>
                </form>
            </div>
        <?php } else { ?>
            <div class="navbar-item-group right xs-12">
                <a class="navbar-item navbar-item-link" style="font-size:small" href="<?php echo ($_SERVER['PHP_SELF']==='index.php')?'.':'..' ?>/Controladoras/UsuarioController.php?handler=redirect"><span class="glyphicon glyphicon-user"></span> <?php echo "{$_SESSION['user']['nombre']}"; ?></a>
                <a class="navbar-item navbar-item-link" href="<?php echo ($_SERVER['PHP_SELF']==='index.php')?'.':'..' ?>/Controladoras/UsuarioController.php?handler=logout"><img src="<?php echo ($_SERVER['PHP_SELF']==='index.php')?'.':'..' ?>/Vistas/img/logout.svg" width="20" height="20" style="vertical-align:middle" alt=""> Salir</a>
            </div> <?php } ?>
        </nav>
</div>
