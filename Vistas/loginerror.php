<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login</title>

    <link rel="stylesheet" href="./css/global.css">
    <link rel="stylesheet" href="./css/login.css">
  </head>
  <body>

      <div class="main-container">

          <section>
              <div class="login">
                  <h1 class="header">Acceso al sistema</h1>
                  <p><span class="glyphicon glyphicon-warning-sign"></span>Su cuenta de usuario no tiene acceso a este módulo del sistema. Por favor, ingrese con los credenciales correctos</p>
                  <form class="form-block">
                      <div class="form-group">
                          <input type="text" name="ced" class="form-control" pattern="" required placeholder="No. Identificación. (ej:102340567)">
                      </div>
                      <div class="form-group">
                          <input type="password" class="form-control" placeholder="Contraseña" required>
                      </div>
                      <hr class="separator">
                      <button type="submit" class="btn btn-primary">Registrarse</button>
                  </form>
              </div>
          </section>

          <footer></footer>
      </div>

  </body>
</html>
