<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title id="Description">Usuarios</title>
    <meta name="description" content="">
    <link rel="stylesheet" href="css/global.css"/>
    <link rel="stylesheet" href="css/inicio.css"/>
    <link rel="stylesheet" href="css/admin.css"/>
    <link rel="stylesheet" href="css/alquilerBusesAdmi.css"/>
    <link rel="stylesheet" href="css/glyphicons.css"/>
    <link rel="stylesheet" href="css/navbar.css"/>
    <link rel="stylesheet" href="css/navbarAdmin.css"/>

    <link href="css/alerts/alertify.css" rel="stylesheet">
    <link href="css/alerts/alertify.core.css" rel="stylesheet">

    <link rel="stylesheet" href="jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="jqwidgets/scripts/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="jqwidgets/jqwidgets/styles/jqx.darkblue.css" type="text/css" />
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxdatatable.js"></script> 
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxlistbox.js"></script> 
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxdropdownlist.js"></script> 
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxtooltip.js"></script> 
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxinput.js"></script> 
    <script type="text/javascript" src="jqwidgets/scripts/demos.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxwindow.js"></script>

    <script type="text/javascript" src="jqwidgets/jqwidgets/globalization/globalize.js"></script>

    <script type="text/javascript" src="js/usuario.js"></script> 
    <script type="text/javascript">
         $(document).ready(function () {
            cargarDatosUsuario();
        });
    </script>
</head>
<body>
 <?php include './partials/navbarAdmin.php'; ?>
        <section id="contendor">
            <div id="dataTable"></div>
            <div style="visibility: hidden;" id="dialog">
            <div>Edit Dialog</div>
            <div style="overflow: hidden;">
                <table style="table-layout: fixed; border-style: none;">
                    <tr>
                        <td align="right">Nombre:
                        </td>
                        <td align="left">
                            <input id="nombre" pattern="[A-Za-z]+" type="text" required/>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">C&eacute;dula:
                        </td>
                        <td align="left">
                            <input id="cedula" pattern="[0-9]+" required/>
                        </td>
                    </tr>"
                    <tr>
                        <td align="right">Clave:
                        </td>
                        <td align="left">
                            <input id="clave" required/>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Puesto:</td>
                        <td align="left">
                            <input id="rol" pattern="[A-Za-z]+" type="text" required/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                             <br />
                             <button id="save">Guardar</button> <button style="margin-left: 5px;" id="cancel">Cancelar</button></td>                    
                       </tr>
                </table>
            </div>
        </section>
        <script type="text/javascript" src="js/admin.js"></script>
        <script src="js/alertify.js"></script>
</body>
</html> 