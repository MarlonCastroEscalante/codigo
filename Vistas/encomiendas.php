<?php session_start();?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="stylesheet" href="css/inicio.css"/>
    <link rel="stylesheet" href="css/global.css"/>
    <link rel="stylesheet" href="css/admin.css"/>
    <link rel="stylesheet" href="css/encomiendas.css"/>
    <link rel="stylesheet" href="css/glyphicons.css"/>
    <link rel="stylesheet" href="css/navbar.css"/>
    <link rel="stylesheet" href="css/navbarAdmin.css"/>
    <link rel="stylesheet" type="text/css" href="css/alquilerBusesAdmi.css">
    <link rel="stylesheet" href="jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="jqwidgets/jqwidgets/styles/jqx.darkblue.css" type="text/css" />
    <script type="text/javascript" src="jqwidgets/scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxdatatable.js"></script> 
    <script type="text/javascript" src="jqwidgets/scripts/demos.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxinput.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="jqwidgets./jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxtooltip.js"></script> 
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxwindow.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/globalization/globalize.js"></script>
    <script type="text/javascript" src="js/encomiendas.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var url = '../Datos/DatosEncomienda.php';
            var codigo=0;
            // prepare the data
            var source =
            {
                dataType: "json",
                dataFields: [
                    { name: 'id', type: 'int' },
                    { name: 'sucursal_salida', type: 'string' },
                    { name: 'sucursal_destino', type: 'string' },
                    { name: 'fecha_enviado', type: 'string' },
                    { name: 'tarifa', type: 'float' },
                    { name: 'comentario', type: 'string' },
                    { name: 'nombre_remitente', type: 'string' }
                ],
                data: {
                    accion:1
                },
                type:"POST",
                url: url,
                addRow: function (rowID, rowData, position, commit) {
                    // synchronize with the server - send insert command
                    // call commit with parameter true if the synchronization with the server is successful 
                    // and with parameter false if the synchronization failed.
                    // you can pass additional argument to the commit callback which represents the new ID if it is generated from a DB.
                    commit(true);
                },
                updateRow: function (rowID, rowData, commit) {
                    // synchronize with the server - send update command
                    // call commit with parameter true if the synchronization with the server is successful 
                    // and with parameter false if the synchronization failed.
                    commit(true);
                },
                deleteRow: function (rowID, commit) {
                    // synchronize with the server - send delete command
                    // call commit with parameter true if the synchronization with the server is successful 
                    // and with parameter false if the synchronization failed.
                    commit(true);
                }
                
            };

            var dataAdapter = new $.jqx.dataAdapter(source, {
                loadComplete: function () {
                    // data is loaded.
                }
            });
            $("#dataTable").jqxDataTable(
            {
                source: dataAdapter,
                pageable: true,
                pagerButtonsCount: 10,
                showtoolbar:true,
                altRows: true,
                theme: 'darkblue',
                filterable: true,
                height: 400,
                filterMode: 'advanced',
                width: 1040,
                ready:function(){
                    $("#dialog").on('close', function () {
                        // enable jqxDataTable.
                        $("#dataTable").jqxDataTable({ disabled: false });
                        $("#tablaPaquetes").empty(); //limpia el tbody
                    });
                    $("#modalInsertar").on('close', function () {
                        // enable jqxDataTable.
                        $("#dataTable").jqxDataTable({ disabled: false });
                        $("#tablaInsertar").empty(); //limpia el tbody
                    });
                    $("#dialog").on('open', function () {
                        //AQUI SE LLENA EL MODAL DE PAQUETES

                        $.ajax({
                            url: "../Datos/DatosEncomienda.php",
                            dataType:"json",
                            data:{
                                handler:"getEncomiendaPaquetes",
                                cod:codigo
                            }, 
                            type:"GET",
                            success: function(result){
                                //aqui se llena la tabla;
                                var paquetes = result.paquetes;
                                 for (var i = 0; i < paquetes.length; i++) {
                                    var str='<tr>'+
                                    '<td>'+(i+1)+'</td>'+
                                    '<td>'+paquetes[i].descripcion+'</td>'+
                                    '<td>'+paquetes[i].peso+'kg</td>'+
                                    '<td>'+paquetes[i].Categoria+'</td>'+
                                    '<td>'+paquetes[i].Tipo+'</td>'+
                                    '<td>'+paquetes[i].alto+'cm x '+paquetes[i].ancho+'cm x '+paquetes[i].largo+'cm</td></tr>';
                                    $("#tablaPaquetes").append(str);
                        }
                                }
                            
                        });
                      //termina el dialog open
                    });
                    $("#modalInsertar").on('open',function(){
                       
                        crearFila(1);
                        
                    });
                    $("#dialog").jqxWindow({
                        resizable: false,
                        position: { left: $("#dataTable").offset().left + 75, top: $("#dataTable").offset().top + 35 },
                        width: 870, height: 230,
                        theme: 'darkblue',
                        autoOpen: false
                    });
                    $("#dialog").css('visibility', 'visible');
                    $("#modalInsertar").jqxWindow({
                        resizable: false,
                        position: { left: $("#dataTable").offset().left + 75, top: $("#dataTable").offset().top + 35 },
                        width: 870, height: 430,
                        theme: 'darkblue',
                        autoOpen: false
                    });
                    $("#modalInsertar").css('visibility', 'visible');

                },
                renderToolbar: function(toolBar)
                {
                    var toTheme = function (className) {
                        if (theme == "") return className;
                        return className + " " + className + "-" + theme;
                    }
                    // appends buttons to the status bar.
                    var container = $("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                    var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                    var addButton = $(buttonTemplate);
                    var cancelButton = $(buttonTemplate);
                    var updateButton = $(buttonTemplate);
                    container.append(addButton);
                    container.append(cancelButton);
                    container.append(updateButton);
                    toolBar.append(container);
                    addButton.jqxButton({cursor: "pointer", enableDefault: false,  height: 25, width: 25 });
                    addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                    addButton.jqxTooltip({ position: 'bottom', content: "Add"});
                    updateButton.find('div:first').addClass(toTheme('jqx-icon-save'));
                    updateButton.jqxTooltip({ position: 'bottom', content: "Save Changes"});
                    cancelButton.jqxButton({ cursor: "pointer", disabled: true, enableDefault: false,  height: 25, width: 25 });
                    cancelButton.find('div:first').addClass(toTheme('jqx-icon-cancel'));
                    cancelButton.jqxTooltip({ position: 'bottom', content: "Cancel"});
                    var updateButtons = function (action) {
                        switch (action) {
                            case "Select":
                                addButton.jqxButton({ disabled: false });
                                cancelButton.jqxButton({ disabled: true });
                                updateButton.jqxButton({ disabled: true });
                                break;
                            case "Unselect":
                                addButton.jqxButton({ disabled: false });
                                cancelButton.jqxButton({ disabled: true });
                                updateButton.jqxButton({ disabled: true });
                                break;
                            case "Edit":
                                addButton.jqxButton({ disabled: true });
                                cancelButton.jqxButton({ disabled: false });
                                updateButton.jqxButton({ disabled: false });
                                break;
                            case "End Edit":
                                addButton.jqxButton({ disabled: false });
                                cancelButton.jqxButton({ disabled: true });
                                updateButton.jqxButton({ disabled: true });
                                break;
                        }
                    }
                    var rowIndex = null;
                    $("#dataTable").on('rowSelect', function (event) {
                        var args = event.args;
                        rowIndex = args.index;
                        updateButtons('Select');
                    });
                    $("#dataTable").on('rowUnselect', function (event) {
                        updateButtons('Unselect');
                    });
                    $("#dataTable").on('rowEndEdit', function (event) {
                        updateButtons('End Edit');
                    });
                    $("#dataTable").on('rowBeginEdit', function (event) {
                        updateButtons('Edit');
                    });
                    addButton.click(function (event) {
                        if (!addButton.jqxButton('disabled')) {
                           /* // add new empty row.
                            $("#dataTable").jqxDataTable('addRow', null, {}, 'first');
                            // select the first row and clear the selection.
                            $("#dataTable").jqxDataTable('clearSelection');
                            $("#dataTable").jqxDataTable('selectRow', 0);
                            // edit the new row.
                            $("#dataTable").jqxDataTable('beginRowEdit', 0);
                            updateButtons('add');*/

                            /*Levantar modal de insertar*/
                            $("#modalInsertar").jqxWindow('setTitle', "Nueva encomienda");
                            $("#modalInsertar").jqxWindow('open');
                            $("#dataTable").jqxDataTable({ disabled: true });
                        }
                    });
                    cancelButton.click(function (event) {
                        if (!cancelButton.jqxButton('disabled')) {
                            // cancel changes.
                            $("#dataTable").jqxDataTable('endRowEdit', rowIndex, true);
                            $("#dataTable").jqxDataTable('deleteRow', rowIndex, true);
                        }
                    });
                    updateButton.click(function (event) {
                        if (!updateButton.jqxButton('disabled')) {
                            // save changes.
                            $("#dataTable").jqxDataTable('endRowEdit', rowIndex, false);
                        }
                    });
            
                },
                columns: [
                  { text: 'Numero de guía', cellsAlign: 'center', align: 'center', dataField: 'id', width: 150 },
                  { text: 'Origen',cellsAlign: 'center', align: 'center', dataField: 'sucursal_salida', width: 150 },
                  { text: 'Destino', cellsAlign: 'center', align: 'center', dataField: 'sucursal_destino', width: 150 },
                  { text: 'Fecha', cellsAlign: 'center', align: 'center', format:'date', dataField: 'fecha_enviado', width: 150 },
                  { text: 'Comentario', cellsAlign: 'center', align: 'center', dataField: 'comentario' },
                  { text: 'Precio', cellsAlign: 'center', align: 'center', dataField: 'tarifa' },
                  { text: 'Remitente', cellsAlign: 'center', align: 'center', dataField: 'nombre_remitente' }
                  
              ]
            });
            $("#dataTable").on('rowDoubleClick', function (event) {
                var args = event.args;
                var index = args.index;
                var row = args.row;
                // update the widgets inside jqxWindow.
                $("#dialog").jqxWindow('setTitle', "Encomienda: " + row.id);
                $("#dialog").jqxWindow('open');
                $("#dialog").attr('data-row', index);
                $("#dataTable").jqxDataTable({ disabled: true });
                codigo=row.id;
            });
        });
    </script>
  </head>
  <body>
             <?php include_once './partials/navbarAdmin.php'; ?>

        <section id="contendor">
          <div id="dataTable"></div>
          <div style="visibility: hidden;" id="dialog">
        <div>Edit Dialog</div>
        <div style="overflow: hidden;">
            <table id="tabla" class="table striped" style="table-layout: fixed; border-style: none;">
                <thead>
                        <tr>
                            <th>#</th>
                            <th>Descripción</th>
                            <th>Peso</th>
                            <th>Categoría</th>
                            <th>Tipo</th>
                            <th>Dimensiones</th>
                        </tr>
                    </thead>
                    <tbody id="tablaPaquetes"></tbody>
            </table>
        </div>
    </div>

    <!-- Modal de insertar paquetes -->
    <div style="visibility: hidden;" id="modalInsertar">
        <div>Edit Dialog</div>
        <div style="overflow: scroll;">
            <form id="formulario" method="POST">
            <table style="table-layout: fixed; border-style: none;">
                <tr>
                    <td align="right">Destino:
                    </td>
                    <td align="left">
                        <select name="destino" id="destino"></select>
                    </td>
                </tr>
                <tr>
                    <td align="right">Comentario:
                    </td>
                    <td align="left">
                        <textarea name="comentario" id="comentario"></textarea>
                    </td>
                </tr>
                <tr>
                    <td align="right">Cedula remitente:
                    </td>
                    <td align="left">
                        <input name="cedulaRemitente" id="cedulaRemitente" type="text"/>
                    </td>
                </tr>
                <tr>
                    <td align="right">Nombre remitente:
                    </td>
                    <td align="left">
                        <input name="nombreRemitente" id="nombreRemitente" type="text"/>
                    </td>
                </tr>
                
            </table>

            <table class="table striped" style="table-layout: fixed; border-style: none;">
                <thead>
                        <tr>
                            <th>Descripción</th>
                            <th>Peso</th>
                            <th>Categoría</th>
                            <th>Dimensiones</th>
                        </tr>
                    </thead>
                    <tbody id="tablaInsertar"></tbody>
            </table>
            </form>
            <!-- <button id="save">Guardar</button>  -->
            <button id="save" onclick="enviarEncomienda()">Guardar</button> 
            <button style="margin-left: 5px;" id="cancel">Cancelar</button>
        
        </div>
    </div>

        </section>
        
       
        <script type="text/javascript" src="js/admin.js"></script>
        <script type="text/javascript" src="js/validations.js"></script>
  </body>
 </html>