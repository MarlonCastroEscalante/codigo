<?php include '../Controladoras/UsuarioController.php'; redirect('p'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="stylesheet" href="./css/rutas.css">
    <link rel="stylesheet" href="css/global.css"/>
    <!--<link rel="stylesheet" href="css/inicio.css"/>
    <link rel="stylesheet" href="css/alquilerBusesAdmi.css"/>-->
    <link rel="stylesheet" href="css/glyphicons.css"/>
    <link rel="stylesheet" href="css/navbar.css"/>
    <title></title>

</head>

<body>
    <?php include_once './partials/navbar2.php'; ?>

    <section id="contendor">
        <section class="routes-container">
            <div class="route-search lg-6 md-6 sm-12 xs-12">

                <header><h1>Búsqueda de rutas y horarios</h1></header>

                <form id="busqueda" onsubmit="return false">
                    <div id="input-wrapper" class="form-group">
                        <button id="fdt" type="button" class="input-button-dropdown" data-target="filter-dropdown" data-toggle="dropdown"><span class="glyphicon glyphicon-road"></span><span style="font-size:x-small" class="glyphicon glyphicon-triangle-bottom"></span></button>
                        <ul id="filter-dropdown" class="dropdown">
                            <li class="dropdown-header">Filtros<hr class="separator"></li>
                            <li class="dropdown-item" onclick="setFilter(0,'#fdt')"><span class="glyphicon glyphicon-road"></span> <span>Rutas</span></li>
                            <li class="dropdown-item" onclick="setFilter(1,'#fdt')"><span class="glyphicon glyphicon-map-marker"></span> <span>Paradas</span></li>
                        </ul>
                        <input id="buscar-ruta" class="form-control" type="text" name="search" placeholder="Búsqueda..." onkeyup="autoCompleteSearch(this)" autocomplete="off">
                        <span class="glyphicon glyphicon-search right"></span>
                        <ul id="iwr"></ul>
                    </div>
                </form>

            <div id="map-container" class="hidden lg-12 md-12 sm-12 xs-12">
                <h3 id="route-title"></h3>
                <p>Elija una ruta para mostrar su recorrido.</p>
                <div id="map-canvas"></div>
            </div>
        </div>

        <div class="result-container push-lg-2 push-md-2 push-sm-0 push-xs-0 lg-4 md-4 sm-12 xs-12">
            <div class="schedule">
                <h3><span onclick="toggleCollapse(this,'#schedule-list')" class="glyphicon glyphicon-menu-down" style="font-size:small;color:#999"></span><span>Horarios</span><hr class="separator"></h3>
                <div id="schedule-list"></div>
            </div>

            <div class="stops">
                <h3><span onclick="toggleCollapse(this,'#stops-list')" class="glyphicon glyphicon-menu-down" style="font-size:small;color:#999"></span><span>Paradas</span><hr class="separator"></h3>
                <div id="stops-list"></div>
            </div>
        </div>
    </section>
</section>
<script type="text/javascript" src="./js/jquery.google.js"></script>
<script type="text/javascript" src="./js/rutas.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtBqquuE1ttZMBTbCl8bnw0GEc0tKGrro&callback=initialize" async defer></script>
<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle]').click(function(){
        var ev=$(this);
        switch (ev.attr('data-toggle')) {
            case 'dropdown':
                dropdownToggle(ev);
                break;
        }
    });
    $('[ui-action="collapse"]').click(function(){
        var t=$('#'+$(this).attr('ui-target'));
        if(!t.hasClass('collapsed')){t.addClass('collapsed');}
        else {t.removeClass('collapsed');}
    });
});
function dropdownToggle(e){
    var t=$('#'+e.attr('data-target'));
    t.fadeToggle({duration:0,start:function(){t.offset({top:e.outerHeight()+e.offset().top,left:e.offset().left});}});
}
</script>
</body>
</html>
