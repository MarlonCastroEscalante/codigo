<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title id="Description">Alquiler de buses</title>
    <meta name="description" content="">
    <link rel="stylesheet" href="css/global.css"/>
    <link rel="stylesheet" href="css/inicio.css"/>
    <link rel="stylesheet" href="css/admin.css"/>
    <link rel="stylesheet" href="css/alquilerBusesAdmi.css"/>
    <link rel="stylesheet" href="css/glyphicons.css"/>
    <link rel="stylesheet" href="css/navbar.css"/>
    <link rel="stylesheet" href="css/navbarAdmin.css"/>

    <link href="css/alerts/alertify.css" rel="stylesheet">
    <link href="css/alerts/alertify.core.css" rel="stylesheet">

    <link rel="stylesheet" href="jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css"/>
    <script type="text/javascript" src="jqwidgets/scripts/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="jqwidgets/jqwidgets/styles/jqx.darkblue.css" type="text/css" />
    <script type="text/javascript" src="jqwidgets/scripts/demos.js"></script> 
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxdatetimeinput.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxcalendar.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxtooltip.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/globalization/globalize.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxinput.js"></script>
    <link rel="stylesheet" href="jqwidgets/jqwidgets/styles/jqx.dark.css" type="text/css" />
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxdatatable.js"></script> 
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxwindow.js"></script>

        <script type="text/javascript" src="jqwidgets/jqwidgets/jqxmenu.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxcheckbox.js"></script>

    <script type="text/javascript" src="js/alquilerBusesAdmi.js"></script> 
    <script type="text/javascript">
         $(document).ready(function () {
            cargarDatosAlquiler();
        });
    </script>
</head>
<body>
 <?php include_once './partials/navbarAdmin.php'; ?>
        <section id="contendor">
            <div id="dataTable"></div>
            <div style="visibility: hidden;" id="dialog">
                <div>Edit Dialog</div>
                <div style="overflow: scroll;">
                    <table id="tabla" class="table striped" style="table-layout: fixed; border-style: none;">
                        <thead>
                                <tr>
                                    <th>Num</th>
                                    <th>Matricula</th>
                                    <th>Tipo</th>
                                    <th>Capacidad</th>
                                    <th>Estado</th>
                                    <th>Descripcion</th>
                                </tr>
                            </thead>
                            <tbody id="tablaBuses"></tbody>
                    </table>
                </div>
            </div>

    <!-- Modal de insertar alquileres -->
            <div style="visibility: hidden;"id="modalInsertarAlquiler">
                <div id="scroll">
                    <table style="table-layout: fixed; border-style: none;">
                        <tr id="tr">
                            <td align="right">Nombre:
                            </td>
                            <td align="left">
                                <input id="nombre" pattern="[A-Za-z]+" type="text" required/>
                            </td>
                            <td align="right">C&eacute;dula:
                            </td>
                            <td align="left">
                                <input id="cedula" pattern="[0-9]+" required/>
                            </td>
                            <td align="left">Monto:
                            </td>
                        </tr>
                        <tr id="tr">
                            <td align="right">Tel&eacute;fono:
                            </td>
                            <td align="left">
                                <input id="tel" pattern="[0-9]+" required/>
                            </td>
                            <td align="right">Tel&eacute;fono Adicional</td>
                            <td align="left">
                                <input id="tel2" pattern="[0-9]+" />
                            </td>
                            <td align="left">
                                <input id="monto" pattern="[0-9]+" />
                            </td>
                        </tr>
                        <tr id="tr">
                            <td align="right">Fecha Inicio</td>
                            <td align="left">
                                <div id="fechaInicio" type="date"></div>
                            </td>
                            <td align="right">Fecha Limite</td>
                            <td align="left">
                                <div id="fechaFin" type="date"></div>
                            </td>
                            </tr><br><br>
                    </table>
                    <div id="dataTableInsertar"></div>
                    <button id="save">Guardar</button> <button style="margin-left: 5px;" id="cancel">Cancelar</button>
                </div>
            </div>
        </section>
        <script type="text/javascript" src="js/admin.js"></script>
        <script src="js/alertify.js"></script>
</body>
</html>  