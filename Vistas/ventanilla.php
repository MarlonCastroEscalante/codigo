<?php include '../Controladoras/UsuarioController.php'; redirect('v'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="stylesheet" href="./css/global.css">
    <link rel="stylesheet" href="./css/ventanilla.css">
    <link rel="stylesheet" href="./css/rutas.css">
    <link rel="stylesheet" href="./css/navbar.css">
    <link rel="stylesheet" href="./css/glyphicons.css">
    <link rel="stylesheet" href="./js/switchery/switchery.css" />

    <title></title>
</head>
<body>

    <?php include_once './partials/navbar2.php'; ?>

    <section class="routes-container">
        <div class="lg-5 md-5 push-lg-0 push-md-0 push-sm-0 push-xs-0 sm-12 xs-12">

            <header><h1>Boletería en ventanilla</h1></header>

            <div class="search-section">
                <form id="busqueda">
                    <div class="form-group">
                        <div id="input-wrapper">
                            <input id="buscar-ruta" class="form-control" type="text" name="search" placeholder="Buscar ruta..." onkeyup="autoCompleteSearch(this)" onfocus="collapseRouteList($(this))" onclick="collapseRouteList($(this))" autocomplete="off">
                            <span class="glyphicon glyphicon-search right"></span>
                            <ul id="iwr"></ul>
                        </div>
                    </div>
                </form>
            </div>

            <div class="schedule">
                <header>
                    <h3 style="margin:0 2em 0 0;padding:0">Horarios</h3>

                </header>
                <span style="font-size:small;font-weight:100">Desde la hora actual/Completo </span><input type="checkbox" id="switchery-checkbox" onchange="cargarHorarios(l)" checked disabled autocomplete='off' value="">
                    <hr class="separator">
                <div id="schedule-list" class="row"></div>
            </div>
        </div>

        <div class="lg-6 md-6 push-lg-1 push-md-1 push-sm-0 push-xs-0 sm-12 xs-12 payment-section">
            <header style="text-indent:1em">
                <h2>Ruta <span id="route-title">-</span></h2>
                <h4>Tarifa: ¢<span id="tarifa-title">0</span></h4>
            </header>
            <hr class="separator">
            <form id="payment-form">
                <input id="tarifa-value" type="hidden" name="tarifa">
                <table>
                    <tbody>
                        <tr>
                            <td><label for="cant-tiquetes">Tiquetes:</label></td>
                            <td><input id="cant-tiquetes" class="form-control" type="number" name="cant" min="1" value="1" autocomplete="off" onsubmit="this.preventDefault()" onkeyup="setTotal(this)"></td>
                        </tr>
                        <tr>
                            <td>
                                <label>Total a pagar:</label>
                            </td>
                            <td>
                                <span style="font-weight:600;font-size:22px">¢</span> <span id="total-label" style="font-size:22px"></span>
                                <input id="monto-total" type="hidden" name="total">
                            </td>
                        </tr>
                        <tr>
                            <td><label for="">Monto recibido:</label></td>
                            <td><input id="efectivo" class="form-control" type="number" name="monto" placeholder="¢ Colones"></td>
                        </tr>
                    </tbody>
                </table>
                <hr class="separator">
                <table id="payment-details" class="table">
                    <!--<caption>Tiquete <hr class="separator"> </caption>-->
                    <tbody>
                        <tr>
                            <td>Origen:</td>
                            <td id="origen-response"></td>
                        </tr>
                        <tr>
                            <td>Destino:</td>
                            <td id="destino-response"></td>
                        </tr>
                        <tr>
                            <td>Hora de salida:</td>
                            <td id="hora-response"></td>
                        </tr>
                        <tr>
                            <td>Tiquetes:</td>
                            <td id="tiquetes-response"></td>
                        </tr>
                        <tr>
                            <td>Impreso:</td>
                            <td id="fecha-response"></td>
                        </tr>
                        <tr>
                            <td>Boletero/a:</td>
                            <td id="boletero-response"></td>
                        </tr>
                    </tbody>
                </table>
                <button id="imprimir" class="btn btn-primary" data-action="submit" onclick="generarTiquete(this)" type="button">Imprimir tiquete</button>
            </form>
        </div>
    </section>

    <script type="text/javascript" src="./js/jquery.google.js"></script>
    <script type="text/javascript" src="./js/ventanilla.js"></script>
    <script src="./js/switchery/switchery.js"></script>
    <script type="text/javascript">
    var rutas={},sw_i=new Switchery($('#switchery-checkbox')[0],{size:'small'}),l=null;
    $(document).ready(function(){

        $('[ui-action="collapse"]').click(function(){
            var t=$('#'+$(this).attr('ui-target'));
            if(!t.hasClass('collapsed')){t.addClass('collapsed');}
            else {t.removeClass('collapsed');}
        });

        $.ajax({
            url:'../Datos/datosRuta.php?handler=testGetAllRoutes',
            type:'get',
            dataType:'json',
            success:function(response){rutas=response;}
        });
    });
    </script>
</body>
</html>
