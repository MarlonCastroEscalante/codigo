<?php session_start();?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="stylesheet" href="css/global.css"/>
    <link rel="stylesheet" href="../inicio.css"/>
    <link rel="stylesheet" href="css/admin.css"/>
    <link rel="stylesheet" href="css/glyphicons.css"/>
    <link rel="stylesheet" href="css/navbar.css"/>
    <link rel="stylesheet" href="css/navbarAdmin.css"/>
    <link rel="stylesheet" href="js/slider/slider.css"/>
    <link type="text/css" rel="Stylesheet" href="jqwidgets/jqwidgets/styles/jqx.base.css" />
    <script src="./js/jquery.google.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxfileupload.js"></script>
    <script type="text/javascript" src="jqwidgets/scripts/demos.js"></script>

    <script type="text/javascript" src="js/recargarImagen.js"></script>
    <script type="text/javascript">
    </script>
 </head>

<body>
	<?php include './partials/navbarAdmin.php'; ?>
    <header class="">
        <ul id="slider" style="height:80%;">
            <li class"img-slider" ><img src="img/slide1.jpg" alt=""></li>
            <li class"img-slider"><img src="img/slide5.jpg" alt=""></li>
            <li class"img-slider"><img src="img/slide7.jpg" alt=""></li>
        </ul>
    </header>
            <script src="./js/slider/slider.js"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('#slider').bxSlider();

                    $('[ui-action="collapse"]').click(function(){
                        var t=$('#'+$(this).attr('ui-target'));
                        if(!t.hasClass('collapsed')){t.addClass('collapsed');}
                        else {t.removeClass('collapsed');}
                    });
                });
            </script>
</body>

</html>
