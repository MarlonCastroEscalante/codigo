<!DOCTYPE html>
<html lang="en">
<head>
    <title id="Description">Alquiler de buses</title>
    <meta name="description" content="This sample demonstrates how we can customize the rendering of the Data Rows in the jQWidgets DataTable widget.">
    <link rel="stylesheet" href="css/global.css"/>
    <link rel="stylesheet" href="css/inicio.css"/>
    <link rel="stylesheet" href="css/admin.css"/>
    <link rel="stylesheet" href="css/busesCliente.css"/>
    <link rel="stylesheet" href="css/glyphicons.css"/>
    <link rel="stylesheet" href="css/navbar.css"/>

   <link rel="stylesheet" href="jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="jqwidgets/jqwidgets/styles/jqx.darkblue.css" type="text/css" />
    <script type="text/javascript" src="jqwidgets/scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxdatatable.js"></script> 
    <script type="text/javascript" src="jqwidgets/scripts/demos.js"></script>
    <script type="text/javascript" src="jqwidgets/jqwidgets/jqxtooltip.js"></script> 

    <script type="text/javascript" src="js/busesCliente.js"></script> 
    <script type="text/javascript">
        $(document).ready(function (){
         cargarTabla();
       });
    </script>
</head>
<body class='default'> 
  <?php include './partials/navbar2.php';?> 
  <section id="contendor">
    <div id="dataTable"></div>
  </section>
   
</body>
</html>