<?php include_once '../Controladoras/UsuarioController.php';redirect('p'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="stylesheet" href="./css/global.css">
    <link rel="stylesheet" href="./css/glyphicons.css">
    <link rel="stylesheet" href="./css/navbar.css">
    <link rel="stylesheet" href="./css/tracking.css">
    <link rel="stylesheet" href="./css/animate.min.css">

    <title>{Nombre aplicacion} | Tracking</title>

</head>
<body>

    <?php include './partials/navbar2.php'; ?>

    <section class="main-container">
        <!--####################### FORM DE CODIGO DE RASTREO #################-->
        <div id="tracking-form-container" class="lg-4 push-lg-4 xs-10 push-xs-1">
            <header>
                <img src="./svg/brujula.svg" height="110" width="110">
                <h3>Rastreo de sus encomiendas.</h3>
            </header>
            <p><span class="glyphicon glyphicon-asterisk" style="color:#F45353"></span> Debe ingresar el código de rastreo que se le proporcionó al efectuar su envío.</p>
            <p class="alert" style="color:#FF9741;display:none"><span class="glyphicon glyphicon-alert"></span> No existe este número de rastreo.</p>
            <p class="error" style="color:#F45353;display:none"><span class="glyphicon glyphicon-remove"></span> <span class="msg"></span></p>
            <form id="tracking-form">
                <input type="hidden" name="handler" value="getEncomiendaPaquetes">
                <div class="form-group">
                    <input id="tracking-code" class="form-control" type="text" name="cod" placeholder="Código de rastreo">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit" onclick="loadDetails()">Mostrar detalles</button>
                </div>
            </form>
        </div>
        <!--#################################################################-->

        <section id="info-section" class="lg-10 push-lg-1 sm-12 push-sm-0">
            <!--####################### INFORMACION DEL CLIENTE #################-->
            <article class="lg-12">
                <details open>
                    <summary class="summary-header"><h4>Información del remitente</h4></summary>
                    <ul id="sender-info"></ul>
                </details>
            </article>
            <!--####################### ################### #####################-->

            <!--####################### INFORMACION DE LA ENCOMIENDA #################-->
            <article class="lg-12">
                <details open>
                    <summary class="summary-header"><h4>Información del envío</h4></summary>
                    <ul id="shipping-info"></ul>
                </details>
            </article>
            <!--####################### ################### #####################-->

            <!--####################### TABLA DE PAQUETES #################-->
            <div class="packages-table lg-12 md-12 sm-12 xs-12">
                <table class="table striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Descripción</th>
                            <th>Peso</th>
                            <th>Categoría</th>
                            <th>Tipo</th>
                            <th>Dimensiones</th>
                        </tr>
                    </thead>
                    <tbody id="package-details"></tbody>
                </table>
            </div>
            <!--####################### ################### #################-->
        </section>
    </section>

    <footer></footer>

    <script src="./js/validations.js"></script>
    <script src="./js/jquery.google.js" charset="utf-8"></script>
    <script type="text/javascript">
        var alert_msg=$('#tracking-form-container .alert'),
        error_msg=$('#tracking-form-container .error');
        $(document).ready(function(){

            $('[ui-action="collapse"]').click(function(){
                var t=$('#'+$(this).attr('ui-target'));
                if(!t.hasClass('collapsed')){t.addClass('collapsed');}
                else {t.removeClass('collapsed');}
            });

            $('#tracking-form').submit(function(e){e.preventDefault();});
        });

        function loadDetails(){
            var cod=$('#tracking-code').val();
            if(isEmpty(cod)){
                error_msg.find('.msg').text('Este campo es requerido.');
                error_msg.fadeIn(300);
                return false;
            }
            if(!isInteger(cod)){
                error_msg.find('.msg').text('Solo se permiten números.');
                error_msg.fadeIn(300);
                return false;
            }

            $.ajax({
                url:'../Datos/DatosEncomienda.php?handler=getEncomiendaPaquetes',
                data:{cod:cod},
                type:'get',
                dataType:'json',
                success:function(response){
                    var encomienda=response.encomienda,
                    paquetes=response.paquetes;

                    if(encomienda){
                        str='<li><b>Nombre del remitente: </b>'+encomienda.nombre_remitente+'</li>'+
                        '<li><b>Cédula: </b>'+encomienda.cedula_remitente+'</li>'+
                        '<li><b>Número de teléfono: </b>'+((!encomienda.telefono_remitente)?'Sin especificar':encomienda.telefono_remitente)+'</li>';
                        $('#sender-info').append(str);

                        str='<li><b>Lugar de salida: </b>'+encomienda.salida+'</li>'+
                        '<li><b>Lugar de llegada: </b>'+encomienda.destino+'</li>'+
                        '<li><b>Enviado por: </b>'+encomienda.emisor+'</li>'+
                        '<li><b>Fecha y hora de envío: </b>'+encomienda.fecha_enviado+'</li>'+
                        '<li><b>Número de paquetes: </b>'+paquetes.length+'</li>'+
                        '<li><b>Tarifa: </b>¢'+encomienda.tarifa+'</li>'+
                        '<li><b>Comentario: </b>'+((encomienda.comentario)?encomienda.comentario:'<span style="color:grey">Ninguno</span>')+'</li>'+
                        '<li><b>Estado: </b>'+((!encomienda.fecha_recibido)?'<span style="color:blue">Enviado </span><span style="color:darkblue" class="glyphicon glyphicon-time"></span></li>':'<span style="color:green">Recibido </span><span style="color:darkgreen" class="glyphicon glyphicon-ok"></span></li>'+
                        '<li><b>Recibido por: </b>'+encomienda.receptor+'</li><li><b>Fecha y hora de recepción: </b>'+encomienda.fecha_recibido+'</li>');
                        $('#shipping-info').append(str);

                        str='';
                        for(var i=0,len=paquetes.length;i<len;i++){
                            str+='<tr>'+
                                    '<td>'+(i+1)+'</td>'+
                                    '<td>'+paquetes[i].descripcion+'</td>'+
                                    '<td>'+paquetes[i].peso+'kg</td>'+
                                    '<td>'+paquetes[i].Categoria+'</td>'+
                                    '<td>'+paquetes[i].Tipo+'</td>'+
                                    '<td>'+paquetes[i].alto+'cm x '+paquetes[i].ancho+'cm x '+paquetes[i].largo+'cm</td></tr>';
                        }
                        $('#package-details').append(str);

                        $('#tracking-form-container').css('display','none');
                        $('#info-section').css('display','initial');
                    }
                    else {
                        $('#tracking-form-container').addClass('animated shake').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){$(this).removeClass('animated shake');});
                        alert_msg.fadeIn(300);
                    }
                }
            });
        }
    </script>
</body>
</html>
