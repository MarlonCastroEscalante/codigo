function cargarTabla(){
	         var url = '../Datos/DatosBus.php';
           var source =
            {
                dataType: "json",
                dataFields: [
                    { name: 'matricula', type: 'string' },
                    { name: 'estado', type: 'string' },
                    { name: 'tipo', type: 'string' },
                    { name: 'imagen', type: 'string' },
                    { name: 'capacidad', type: 'string' },
                    { name: 'descripcion', type: 'string' }
                ],
                data: {
                    accion:1
                },
                type:"POST",
                url: url,
                
                success:function(data){
                    alert(data);
                },
                error:function(){
                    alert("ERROR");
                }
            };
         
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#dataTable").jqxDataTable(
            {
                width: 850,
                source: dataAdapter,
                sortable: true,
                pageable: true,
                pageSize: 4,
                theme: 'darkblue',
                filterable: true,
                filterMode: 'simple',
                pagerButtonsCount: 5,

                columns: [
                      {
                          text: 'Imagen', align: 'center', dataField: 'matricula', width: 150, height: 1500,
                          cellsRenderer: function (row, column, value, dataField) {
                              var image = "<div style='margin: 5px; margin-bottom: 3px;'>";
                              var imgurl = 'img/' + dataField.imagen;
                              var img = '<img width="130" height="60" style="display: block;" src="' + imgurl + '"/>';
                              image += img;
                              image += "</div>";
                              return image;
                          }
                      },
                      {
                          text: 'Detalles', align: 'center', dataField: 'descripcion',
                          cellsRenderer: function (row, column, value, dataField) {
                              var container = '<div style="width: 100%; height: 100%;">'
                              var leftcolumn = '<div style="float: left; width: 50%;">';
                              var rightcolumn = '<div style="float: left; width: 50%;">';
                      
                              var tipo = "<div style='margin: 10px;'><b>Tipo:</b> " + dataField.tipo + "</div>";
                              var estado = "<div style='margin: 10px;'><b>Estado:</b> " + dataField.estado + "</div>";
                            
                              leftcolumn += tipo;
                              leftcolumn += estado;
                              leftcolumn += "</div>";

                              var capacidad = "<div style='margin: 10px;'><b>Capacidad:</b> " + dataField.capacidad + "</div>";
                              var descripcion = "<div style='margin: 10px;'><b>Descripci&oacute;n:</b> " + dataField.descripcion + "</div>";

                              rightcolumn += capacidad;
                              rightcolumn += descripcion;
                              rightcolumn += "</div>";

                              container += leftcolumn;
                              container += rightcolumn;
                              container += "</div>";
                              return container;
                          }
                      }
                ]
            });
}