
function crearFila($numeroFila){
    
    var tbody = "<tr><td><input name='descripcion"+$numeroFila+"' id='descripcion' type='text'/></td><td><input name='peso"+$numeroFila+"' id='peso' style='width: 50%;"+
                                    "margin-right: 2%;' type='text'/>Kg</td><td><select name='categoria"+$numeroFila+"' class='categoria'>"+
                                    "</select></td><td><select name='dimensiones"+$numeroFila+"' class='dimensiones'></select></td><td><span><a href='javascript:crearFila("+($numeroFila+1)+")'>+</a></span></td></tr>";
    $("#tablaInsertar").append(tbody);

    $.ajax({
            url: "../Datos/DatosSucursal.php",
            dataType:"json",
            data:{
                   accion:1
                 },
             type:"POST",
             success:function(datos){
                     var sucursales = datos;
                     var str ="";
                      for(var i = 0; i < sucursales.length; i++){
                           str +="<option value='"+sucursales[i].codigo+"'>"+sucursales[i].ubicacion+"</option>";
                          }
                     $("#destino").append(str);
                      }
     });

    $.ajax({
            url: "../Datos/DataExtrasPaquetes.php",
            dataType:"json",
            data:{
                   accion:1
                 },
             type:"POST",
             success:function(datos){
                     var categorias = datos;
                     var str ="";
                      for(var i = 0; i < categorias.length; i++){
                           str +="<option value='"+categorias[i].id+"'>"+categorias[i].descripcion+"</option>";
                          }
                     $(".categoria").append(str);
                      }
     });

    $.ajax({
            url: "../Datos/DataExtrasPaquetes.php",
            dataType:"json",
            data:{
                   accion:2
                 },
             type:"POST",
             success:function(datos){
                     var dimensiones = datos;
                     var str ="";
                      for(var i = 0; i < dimensiones.length; i++){
                           str +="<option value='"+dimensiones[i].id+"'>"+dimensiones[i].descripcion+"</option>";
                          }
                     $(".dimensiones").append(str);
                      }
     });
        
}

function enviarEncomienda(){
    var cantPaquetes = $("#tablaInsertar tr").length;

    $.ajax({
        url:"../Controladoras/ControladoraEncomienda.php",
        dataType:"json",
        data:{
            formulario: $("#formulario").serialize(),
            cantPaquetes:cantPaquetes,
            accion:1
        },
        type:"POST",
        success:function(datos){
            //Ya con el paquete armado llamo al data
            $.ajax({
                url:"../Datos/DatosEncomienda.php",
                data:{
                    encomienda:datos,
                    accion:2
                },
                type:'POST',
                success:function(data){
                    alert(data);
                }
            });
        }
    });
}
