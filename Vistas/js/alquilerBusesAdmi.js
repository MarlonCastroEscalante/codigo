function insertarAlquiler(alquiler){
    //alert(alquiler);
        alertify.confirm('¿Seguro que desea continuar?','',function(){
        $.ajax({
            url:'../Datos/DatosAlquiler.php',
            data: {accion:3,alquiler:alquiler},
            type:'post',
            success:function(data) {
                cargarDatosAlquiler();
                alerta("<b>La transaccion se realizó con exito</b>", "success");
            },
            error: function(){
            alerta("<b>Error al insertar</b>", "Error");
        }
        });

        }, function(){
        alertify.error('Cancelado')});
}

function eliminarAlquiler(codigo){
    alertify.confirm('¿Seguro que desea continuar?','',function(){
    //alertify.success('Eliminando');
    $.ajax({
        url:'../Datos/DatosAlquiler.php',
        data: {accion:2,codigo:codigo},
        type:'post',
        success:function(data) {
            cargarDatosAlquiler();
            alerta("<b>Eliminado con exito</b>", "success");
        },
        error: function(){
            alerta("<b>Error al eliminar</b>", "Error");
        }
    });

        }, function(){
        alertify.error('Cancelado')});
}

function cargarDatosAlquiler(){
    var url = '../Datos/DatosAlquiler.php';
    //var codigo=1;
            var source =
            {
                dataType: "json",
                dataFields: [
                    { name: 'codigo', type: 'int' },
                    { name: 'matricula_bus', type: 'string' },
                    { name: 'nombre_cliente', type: 'string' },
                    { name: 'cedula_cliente', type: 'string' },
                    { name: 'telefono_cliente', type: 'string' },
                    { name: 'telefono_cliente2', type: 'string' },
                    { name: 'fecha_alquiler', type: 'string' },
                    { name: 'fecha_final', type: 'string' },
                    { name: 'monto', type: 'float' },
                    { name: 'pagoefectuado', type: 'float' }
                ],
                data: {
                    accion:1
                },
                type:"POST",
                url: url,
                addRow: function (rowID, rowData, position, commit) {
                    commit(true);
                },
                updateRow: function (rowID, rowData, commit) {
                    commit(true);
                },
                deleteRow: function (rowID, commit) {
                    commit(true);
                },
                success:function(data){
                    alert(data);
                },
                error:function(){
                    alert("ERROR");
                }
                
            };

            var dataAdapter = new $.jqx.dataAdapter(source, {
                loadComplete: function () {
                }
            });
            $("#dataTable").jqxDataTable(
            {
                source: dataAdapter,
                pageable: true,
                pagerButtonsCount: 5,
                pageSize: 7,
                showtoolbar:true,
                altRows: true,
                theme: 'darkblue',
                filterable: true,
                height: 300,
                width: 1040,
                ready:function(){

                    $("#dialog").on('close', function () {
                        // enable jqxDataTable.
                        $("#dataTable").jqxDataTable({ disabled: false });
                        $("#tablaBuses").empty(); //limpia el tbody
                    });
                    $("#modalInsertarAlquiler").on('close', function () {
                        // enable jqxDataTable.
                        $("#dataTable").jqxDataTable({ disabled: false });
                        //$("#dataTableInsertar").empty(); //limpia el tbody
                    });
                    $("#dialog").on('open', function () {
                        //AQUI SE LLENA EL MODAL DE ALQUILERES
                       // var codigo = dataField: 'id';
                        $.ajax({
                            url: "../Datos/DatosAlquiler.php",
                            dataType:"json",
                            data:{
                                handler:"getBusesAlquiler",
                                cod:codigo
                            }, 
                            type:"GET",
                            success: function(result){
                                //aqui se llena la tabla;
                                var buses = result.buses;
                                 for (var i = 0; i < buses.length; i++) {
                                    var str='<tr>'+
                                    '<td>'+(i+1)+'</td>'+
                                    '<td>'+buses[i].matricula+'</td>'+
                                    '<td>'+buses[i].tipo+'</td>'+
                                    '<td>'+buses[i].capacidad+'</td>'+
                                    '<td>'+buses[i].estado+'</td>'+
                                    '<td>'+buses[i].descripcion+'</td>';
                                    $("#tablaBuses").append(str);
                        }
                                }
                            
                        });
                      //termina el dialog open
                    });
                    $("#dialog").jqxWindow({
                        resizable: false,
                        position: { left: $("#dataTable").offset().left + 120, top: $("#dataTable").offset().top -50 },
                        width: 870, height: 400,
                        theme: 'darkblue',
                        autoOpen: false
                    });
                    $("#dialog").css('visibility', 'visible');
                    $("#modalInsertarAlquiler").jqxWindow({
                        resizable: false,
                        theme: 'darkblue',
                        position: { left: $("#dataTable").offset().left + 120, top: $("#dataTable").offset().top - 50 },
                        width: 900, height: 400,
                        autoOpen: false
                    });
                    $("#modalInsertarAlquiler").css('visibility', 'visible');

                },
                renderToolbar: function(toolBar)
                {
                    var toTheme = function (className) {
                        if (theme == "") return className;
                        return className + " " + className + "-" + theme;
                    }
                    var container = $("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                    var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                    var addButton = $(buttonTemplate);
                    var cancelButton = $(buttonTemplate);
                    var updateButton = $(buttonTemplate);
                    var editButton = $(buttonTemplate);
                    var deleteButton = $(buttonTemplate);
                    container.append(addButton);
                    container.append(editButton);
                    container.append(deleteButton);
                    toolBar.append(container);
                    addButton.jqxButton({cursor: "pointer", enableDefault: false,  height: 25, width: 25 });
                    addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                    addButton.jqxTooltip({ position: 'bottom', content: "Nuevo"});
                    editButton.jqxButton({ cursor: "pointer", disabled: true, enableDefault: false,  height: 25, width: 25 });
                    editButton.find('div:first').addClass(toTheme('jqx-icon-edit'));
                    editButton.jqxTooltip({ position: 'bottom', content: "Editar"});
                    deleteButton.jqxButton({ cursor: "pointer", disabled: true, enableDefault: false,  height: 25, width: 25 });
                    deleteButton.find('div:first').addClass(toTheme('jqx-icon-delete'));
                    deleteButton.jqxTooltip({ position: 'bottom', content: "Finalizar"});
                    var updateButtons = function (action) {
                        switch (action) {
                            case "Select":
                                addButton.jqxButton({ disabled: false });
                                deleteButton.jqxButton({ disabled: false });
                                editButton.jqxButton({ disabled: false });
                                break;
                            case "Unselect":
                                addButton.jqxButton({ disabled: false });
                                deleteButton.jqxButton({ disabled: true });
                                editButton.jqxButton({ disabled: true });
                                break;
                        }
                    }
                    var rowIndex = null;
                    $("#dataTable").on('rowSelect', function (event) {
                        var args = event.args;
                        rowIndex = args.index;
                        updateButtons('Select');
                    });
                    $("#dataTable").on('rowUnselect', function (event) {
                        updateButtons('Unselect');
                    });
                    addButton.click(function (event) {
                        if (!addButton.jqxButton('disabled')) {
                            // add new empty row.
                            $("#modalInsertarAlquiler").jqxWindow('setTitle', "Nuevo Alquiler");
                            $("#modalInsertarAlquiler").jqxWindow('open');
                            $("#nombre").jqxInput({disabled: false,width: 150, height: 30});
                            $("#cedula").jqxInput({disabled: false,width: 150, height: 30});
                            $("#tel").jqxInput({disabled: false,width: 150, height: 30});
                            $("#tel2").jqxInput({disabled: false,width: 150, height: 30}); 
                            cargarBusesAlquiler('insertar');
                            $("#dataTable").jqxDataTable({ disabled: true });
                        }
                    });
                    editButton.click(function (event) {
                        if (!editButton.jqxButton('disabled')) {
                            var row = args.row;
                            $("#modalInsertarAlquiler").jqxWindow('setTitle', "Editar Alquiler");
                            $("#modalInsertarAlquiler").jqxWindow('open');

                            $("#nombre").jqxInput({disabled: true,width: 150, height: 30 });
                            $("#cedula").jqxInput({disabled: true,width: 150, height: 30 });
                            $("#tel").jqxInput({disabled: true,width: 150, height: 30 });
                            $("#tel2").jqxInput({disabled: true,width: 150, height: 30 }); 
                            $("#nombre").val(row.nombre_cliente);
                            $("#cedula").val(row.cedula_cliente);
                            $("#tel").val(row.telefono_cliente);
                            $("#tel2").val(row.telefono_cliente2);
                            $("#fechaInicio").val(row.telefono_cliente2);
                            $("#fechaFin").val(row.telefono_cliente2);
                            $("#monto").val(row.telefono_cliente2);
                            cargarBusesAlquiler('modificar');
                            //$("#dataTable").jqxDataTable('beginRowEdit', rowIndex);
                            updateButtons('edit');
                        }
                    });
                    deleteButton.click(function () {
                        if (!deleteButton.jqxButton('disabled')) {
                           // $("#dataTable").jqxDataTable('deleteRow', rowIndex);
                            var row = args.row;
                            eliminarAlquiler(row.codigo);
                            updateButtons('delete');
                        }
                    });
                },
                columns: [
                  { text: 'Nombre', cellsAlign: 'center', align: 'center', dataField: 'codigo', dataField: 'nombre_cliente', width: 130 },
                  { text: 'C&eacute;dula',cellsAlign: 'center', align: 'center', dataField: 'cedula_cliente', width: 130 },
                  { text: 'Tel&eacute;fono', cellsAlign: 'center', align: 'center', dataField: 'telefono_cliente', width: 130 },
                  { text: 'Tel&eacute;fono', cellsAlign: 'center', align: 'center', dataField: 'telefono_cliente2', width: 130 },
                  { text: 'Fecha Inicio', cellsAlign: 'center', align: 'center', dataField: 'fecha_alquiler', width: 130 },
                  { text: 'Fecha Final', cellsAlign: 'center', align: 'center', dataField: 'fecha_final' , width: 130 },
                  { text: 'Monto', cellsAlign: 'center', align: 'center', dataField: 'monto' , width: 130 },
                  { text: 'Pago Efectuado', cellsAlign: 'center', align: 'center', dataField: 'pagoefectuado' , width: 130 }
              ]
            });
             $("#dataTable").on('rowDoubleClick', function (event) {
                var args = event.args;
                var index = args.index;
                var row = args.row;
                // update the widgets inside jqxWindow.
                $("#dialog").jqxWindow('setTitle', "Alquiler: " + row.codigo);
                $("#dialog").jqxWindow('open');
                $("#dialog").attr('data-row', index);
                $("#dataTable").jqxDataTable({ disabled: true });
                codigo=row.codigo;
            });
}


function cargarBusesAlquiler(opcion){
    var rowData = {};
    var rowMatricula=[];
    var cant = 0;
    var url = '../Datos/DatosBus.php';
           var source =
            {
                dataType: "json",
                dataFields: [
                    { name: 'matricula', type: 'string' },
                    { name: 'estado', type: 'string' },
                    { name: 'tipo', type: 'string' },
                    { name: 'imagen', type: 'string' },
                    { name: 'capacidad', type: 'string' },
                    { name: 'descripcion', type: 'string' }
                ],
                data: {
                    accion:1
                },
                type:"POST",
                url: url,
                
                success:function(data){
                    alert(data);
                },
                error:function(){
                    alert("ERROR");
                }
            };
    var dataAdapter = new $.jqx.dataAdapter(source);
            var allRowsSelected = function () {
                var selection = $("#dataTableInsertar").jqxDataTable('getSelection');
                var rows = $("#dataTableInsertar").jqxDataTable('getRows');
                if (selection.length == 0) {
                    return false;
                }
                if (rows.length != selection.length) {
                    return null;
                }
              
                return true;
            }
            var updatingSelection = false;
            var updatingSelectionFromDataTable = false;
            $("#dataTableInsertar").jqxDataTable(
            {
                width: 700,
                source: dataAdapter,
                sortable: true,
                pageable: true,
                pageSize: 3,
                theme: 'darkblue',
                filterable: true,
                filterMode: 'simple',
                pagerButtonsCount: 5,
                ready: function () {
                    var f = new Date();
                    //document.write(f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
                   
                    $("#fechaInicio").jqxDateTimeInput({ min: new Date(f.getFullYear(), f.getMonth(),f.getDate()), template: "primary", width: 150, height: 30 ,dropDownHorizontalAlignment: 'right', animationType: 'fade'});
                    $("#fechaFin").jqxDateTimeInput({ min: new Date(f.getFullYear(), f.getMonth(),f.getDate()), template: "primary", width: 150, height: 30 ,dropDownHorizontalAlignment: 'right', animationType: 'fade'});
                    $("#monto").jqxInput({width: 150, height: 30 });
                    $("#save").jqxButton({ height: 30, width: 80 });
                    $("#cancel").jqxButton({ height: 30, width: 80 });
                    $("#cancel").mousedown(function () {
                        // close jqxWindow.
                        $("#modalInsertarAlquiler").jqxWindow('close');
                    });

                    $("#save").mousedown(function () {
                        // close jqxWindow.
                        
                       // 
                        // update edited row.
                        
                        rowData = {
                            nombre_cliente: $("#nombre").val(), cedula_cliente: $("#cedula").val(),
                            telefono_cliente: $("#tel").val(), telefono_cliente2: $("#tel2").val(),
                            fecha_alquiler: $("#fechaInicio").val(), fecha_final: $("#fechaFin").val(),
                            monto: $("#monto").val(), buses:rowMatricula,cont:cant-1
                        };
                        cant = 0;
                        rowMatricula=[];
                        //alert('entre');
                        insertarAlquiler(rowData);
                        $("#modalInsertarAlquiler").jqxWindow('close');
                        //$("#dataTable").jqxDataTable('updateRow', editRow, rowData);
                    });

                    $("#modalInsertarAlquiler").on('close', function () {
                        //alert('Cerrar');
                        // enable jqxDataTable.
                        $("#dataTable").jqxDataTable({disabled: false });
                        $("#nombre").empty();
                        $("#cedula").empty();
                        $("#tel").empty();
                        $("#tel2").empty();
                        $("#monto").empty();
                    });
                },
                columns: [
                      {
                          text: 'Imagen', align: 'center', dataField: 'matricula', width: 150, height: 1500,
                          
                          renderer: function (text, align, height) {
                              var checkBox = "<div id='checkbox' style='z-index: 999; margin: 5px; margin-left: 30px; margin-top: 8px; margin-bottom: 3px;'>";
                              checkBox += "</div>";
                              return checkBox;
                          },
                          /*rendered: function (element, align, height) {
                              element.jqxCheckBox();
                              element.on('change', function (event) {
                                  if (!updatingSelectionFromDataTable) {
                                      var args = event.args;
                                      var rows = $("#dataTableInsertar").jqxDataTable('getRows');
                                      updatingSelection = true;
                                      if (args.checked) {
                                          for (var i = 0; i < rows.length; i++) {
                                              $("#dataTableInsertar").jqxDataTable('selectRow', i);
                                          }
                                      }
                                      else {
                                          $("#dataTableInsertarv").jqxDataTable('clearSelection');
                                      }
                                      updatingSelection = false;
                                  }
                               });
                              return true;
                          },*/
                          cellsRenderer: function (row, column, value, dataField) {
                              var image = "<div style='margin: 5px; margin-bottom: 3px;'>";
                              var imgurl = 'img/' + dataField.imagen;
                              var img = '<img width="130" height="60" style="display: block;" src="' + imgurl + '"/>';
                              image += img;
                              image += "</div>";
                              return image;
                          }
                      },
                      {
                          text: 'Detalles', align: 'center', dataField: 'descripcion',
                          cellsRenderer: function (row, column, value, dataField) {
                              var container = '<div style="width: 100%; height: 100%;">'
                              var leftcolumn = '<div style="float: left; width: 50%;">';
                              var rightcolumn = '<div style="float: left; width: 50%;">';
                      
                              var tipo = "<div style='margin: 10px;'><b>Tipo:</b> " + dataField.tipo + "</div>";
                              var estado = "<div style='margin: 10px;'><b>Estado:</b> " + dataField.estado + "</div>";
                            
                              leftcolumn += tipo;
                              leftcolumn += estado;
                              leftcolumn += "</div>";

                              var capacidad = "<div style='margin: 10px;'><b>Capacidad:</b> " + dataField.capacidad + "</div>";
                              var descripcion = "<div style='margin: 10px;'><b>Descripci&oacute;n:</b> " + dataField.descripcion + "</div>";

                              rightcolumn += capacidad;
                              rightcolumn += descripcion;
                              rightcolumn += "</div>";

                              container += leftcolumn;
                              container += rightcolumn;
                              container += "</div>";
                              return container;
                          }
                      }
                ]
            });
            $("#dataTableInsertar").on('rowSelect', function (event) {
                updatingSelectionFromDataTable = true;
                if (!updatingSelection && $("#checkbox").length > 0) {
                    $cont = $("#checkbox").length;
                    //alert($cont);
                    var row = args.row;
                    rowMatricula[cant]=[row.matricula];
                    cant++;                    
                    //alert(row.matricula+' cant '+cant);
                    $("#checkbox").jqxCheckBox({ checked: allRowsSelected() });
                }
                updatingSelectionFromDataTable = false;
            });
            $("#dataTableInsertar").on('rowUnselect', function (event) {
                updatingSelectionFromDataTable = true;
                if (!updatingSelection && $("#checkbox").length > 0) {
                     var row = args.row;
                    for(var i=0; i<rowMatricula.length;i++){
                        //alert(rowMatricula[i]+' for '+row.matricula);
                        if(rowMatricula[i] == row.matricula){
                            //alert('borre '+rowMatricula.length);
                            cant = cant-1;
                            rowMatricula.splice(i,1);
                        }
                    }
                    //alert('quite');
                    $("#checkbox").jqxCheckBox({ checked: allRowsSelected() });
                }
                updatingSelectionFromDataTable = false;
            });
}

function alerta(dialog, evento){
      alertify.notify(dialog, evento, 5);
}