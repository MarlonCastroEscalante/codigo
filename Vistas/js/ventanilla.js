
function setTotal(e){
    var total=Number(e.value) * Number($('#tarifa-value').val());
    $('#total-label').text(total);

}

function autoCompleteSearch(e){
    var rl=$('#iwr'),
        val=$(e).val();
    if(val!=='' && val!==null){
        rl.empty();
        var str='';
        for (var i = 0, len=rutas.length; i < len; i++) {
            var reg= new RegExp(val.toLowerCase());
            if(reg.test(rutas[i].salida.toLowerCase()) || reg.test(rutas[i].destino.toLowerCase())){str+='<li class="list-group-item" data-ruta-id="'+rutas[i].id+'" data-ruta-salida="'+rutas[i].salida+'" data-ruta-destino="'+rutas[i].destino+'" data-tarifa="'+rutas[i].tarifa+'" onclick="cargarHorarios($(this))">De <b>'+rutas[i].salida+'</b> a <b>'+rutas[i].destino+'</b></li>';}

        }
        rl.append(str);
        rl.offset({top:($(e).offset().top + $(e).outerHeight()),left:$(e).offset().left});
    }
}

function generarTiquete(e){
    if($(e).attr('data-action')==='clear'){
        $('#imprimir').text('Aceptar').removeClass('btn-primary').addClass('btn-success')
        $('#origen-response').empty();
        $('#destino-response').empty();
        $('#tiquetes-response').empty();
        $('#boletero-response').empty();
        $('#fecha-response').empty();
        $('#payment-details').fadeOut();
        $(e).text('Imprimir tiquete').removeClass('btn-success').addClass('btn-primary').attr('data-action','submit');
    }
    else {
        if(l!==null){
            $.ajax({
                url:'../Datos/DatosTiquete.php?handler=getTiqueteInfo',
                data:{efectivo:Number($('#efectivo').val()),total:Number($('#total-label').text())},
                type:'post',
                dataType:'json',
                success:function(response){
                    console.log(response);
                    if(!response.errors){
                        $('#origen-response').text(l.attr('data-ruta-salida'));
                        $('#destino-response').text(l.attr('data-ruta-destino'));
                        $('#tiquetes-response').text($('#cant-tiquetes').val());
                        $('#boletero-response').text(response.user);
                        $('#fecha-response').text(response.fecha);
                        $('#payment-details').fadeIn();
                        $(e).text('Aceptar').removeClass('btn-primary').addClass('btn-success').attr('data-action','clear');
                    }
                    else{
                        alert('El monto recibido no puede ser menor al costo total');
                    }
                }
            });
        }
        else {
            alert('Debe seleccionar una ruta');
        }
    }
}

function collapseRouteList(e){
    var rl=$('#iwr').css('width',$('#input-wrapper').outerWidth()+'px');
        rl.empty();
        var str='';
        for (var i = 0, len=rutas.length; i < len; i++) {str+='<li class="list-group-item" data-ruta-id="'+rutas[i].id+'" data-ruta-salida="'+rutas[i].salida+'" data-ruta-destino="'+rutas[i].destino+'" data-tarifa="'+rutas[i].tarifa+'" onclick="cargarHorarios($(this))">De <b>'+rutas[i].salida+'</b> a <b>'+rutas[i].destino+'</b></li>';}
        rl.append(str);
        rl.offset({top:($(e).offset().top + $(e).outerHeight()),left:$(e).offset().left});
}

function cargarHorarios(e){
    l=e;
    var tipo=$('#switchery-checkbox').is(':checked');
    $('#route-title').text(l.attr('data-ruta-salida')+' - '+l.attr('data-ruta-destino'));
    $('#tarifa-title').text(l.attr('data-tarifa'));
    $('#tarifa-value').val(l.attr('data-tarifa'));
    $('#total-label').text(l.attr('data-tarifa'));
    $('#buscar-ruta').val(l.attr('data-ruta-salida')+' a '+l.attr('data-ruta-destino'));
    $('#schedule-list').empty();
    $.ajax({
        url:'../Datos/datosRuta.php?handler=testGetAllRouteData',
        data:{id:l.attr('data-ruta-id'),tipo:tipo},
        type:'get',
        dataType:'json',
        success:function(response){
            sw_i.enable();
            var str1='';
            for (var i = 0; i < response.horarios.length; i++) {
                if(undefined!==response.horarios[i].horas[0]){str1+='<div style="padding:0 2em" class="left"><h4 style="color:#444">'+response.horarios[i].comienzo+((response.horarios[i].final!==null)?' a '+response.horarios[i].final:'')+'</h4><ul class="list-group"><li class="list-group-item'+((!tipo)?' active':'')+'">'+response.horarios[i].horas[0].hora+'</li>';}
                    for (var k = 1; k < response.horarios[i].horas.length; k++) {
                        str1+='<li class="list-group-item">'+response.horarios[i].horas[k].hora+'</li>';
                    }
                    str1+='</ul></div>';
            }
            $('#iwr li').remove();
            $('#schedule-list').append(str1);
        }
    });
}
