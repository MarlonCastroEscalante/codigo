function animatedModal(options){

    var body=$('body');
    this.settings=$.extend(true,{
        overlay:{show:true,opacity:'.8',closeOnClick:false},
        closeImg:'/closebt.svg',
        target:'animatedModal',
        customClass:'',
        html:'',
        desc:'',
        title:'',
        animatedIn:'bounceInUp',
        animatedOut:'bounceOutDown',
        style:{
            'position':'fixed',
            'height':'initial',
            'width':'40%',
            'bottom':'0',
            'left':'27.5vw', //can be computed.
            'background-color':'white',
            'overflow-y':'hidden',
            'z-index':'2000',
            '-webkit-animation-duration':'.6s',
            '-moz-animation-duration':'.6s',
            '-ms-animation-duration':'.6s',
            'animation-duration':'.6s'
        },
        // Callbacks
        beforeOpenAnim: function() {},
        beforeOpen: function() {},
        afterOpen: function() {},
        beforeClose: function() {},
        afterClose: function() {}
    }, options);
    var overlay=$($.parseHTML('<div '+((settings.overlay.closeOnClick)?'onclick="hide()"':'')+' id="'+settings.target+'-overlay" style="display:none;z-index:1000;background-color:rgba(0,0,0,'+settings.overlay.opacity+');opacity:'+((settings.overlay.show)?1:0)+';width:100%;height:100%;position:fixed;top:0;left:0"></div>')),
    DOM=$($.parseHTML('<div id="'+settings.target+'" class="animated '+settings.customClass+'" style="display:none">'+
    '<div class="close-'+settings.target+'">'+
    '<img alt="close" src="'+settings.closeImg+'" height="40" width="40" class="center-block" onclick="hide()">'+
    '<h3 class="title">'+settings.title+'</h3>'+
    '<p class="desc">'+settings.desc+'</p>'+
    '</div>'+
    '<div class="content-'+settings.target+'">'+settings.html+'</div>'+
    '</div>'));

    body.append(overlay);
    body.append(DOM);

    this.show=function(){
        settings.beforeOpen(); //this may be used to check if another modal is opened

        if(settings.overlay.show){overlay.css('opacity','1')}
        overlay.css('display','initial');
        settings.style.display='initial';
        DOM.css(settings.style);

        settings.beforeOpenAnim();

        DOM.addClass(settings.animatedIn);
        DOM.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', settings.afterOpen);
    }

    this.hide=function(){
        settings.beforeClose();
        DOM.addClass(settings.animatedOut);
        DOM.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            DOM.css('display','none');
            DOM.removeClass(settings.animatedOut);
            overlay.css('display','none');
            settings.afterClose();
        });
    }

    return this;
}
