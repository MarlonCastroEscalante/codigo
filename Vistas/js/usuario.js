function insertarModificarUsuario(usuario){
        alertify.confirm('¿Seguro que desea continuar?','',function(){
        var res=0;
        $.ajax({
            url:'../Datos/DatosUsuario.php',
            data: {accion:3,usuario:usuario},
            type:'post',
            success:function(data) {
                cargarDatosUsuario();
                alerta("<b>La transaccion se realizó con exito</b>", "success");
            }
        });

        }, function(){
        alertify.error('Cancelado')});
}

function eliminarUsuario(cedula){
    alertify.confirm('¿Seguro que desea continuar?','',function(){
    //alertify.success('Eliminando');
    $.ajax({
        url:'../Datos/DatosUsuario.php',
        data: {accion:4, cedula:cedula},
        type:'post',
        success:function(data) {
            alerta("<b>Eliminado con exito</b>", "success");
        },

    error: function(){
            alerta("<b>Error al eliminar</b>", "Error");
        }
    });

    }, function(){
    alertify.error('Cancelado')});
}


function cargarDatosUsuario(){
	var url = '../Datos/DatosUsuario.php';
    var accion = "";
            var source =
            {
                dataType: "json",
                dataFields: [
                    { name: 'cedula', type: 'int' },
                    { name: 'nombre', type: 'string' },
                    { name: 'clave', type: 'string' },
                    { name: 'rol', type: 'string' }
                ],
                data: {
                    accion:2
                },
                type:"POST",
                url: url,
                addRow: function (rowID, rowData, position, commit) {
                    commit(true);
                },    
                updateRow: function (rowID, rowData, commit) {
                    commit(true);
                },
                deleteRow: function (rowID, commit) {
                    commit(true);
                }
            };
            var dataAdapter = new $.jqx.dataAdapter(source, {
                loadComplete: function () {
                }
            });
            $("#dataTable").jqxDataTable(
            {
                width: 1040,
                height: 300,
                source: dataAdapter,
                pagerButtonsCount: 5,
                pageSize: 7,
                pageable: true,
                showToolbar: true,
                theme: 'darkblue',
                altRows: true,
                ready: function()
                {
                    //$("#dataTable").jqxDataTable('lockRow', 1);
                    $("#nombre").jqxInput({width: 150, height: 30 });
                    $("#cedula").jqxInput({width: 150, height: 30 });
                    $("#clave").jqxInput({width: 150, height: 30 });
                    $("#rol").jqxInput({width: 150, height: 30 });
                    $("#save").jqxButton({ height: 30, width: 80 });
                    $("#cancel").jqxButton({ height: 30, width: 80 });
                    $("#cancel").mousedown(function () {
                        // close jqxWindow.
                        $("#dialog").jqxWindow('close');
                    });

                    $("#save").mousedown(function () {
                        // close jqxWindow.
                        $("#dialog").jqxWindow('close');
                        // update edited row.
                        var editRow = parseInt($("#dialog").attr('data-row'));
                        var rowData = {
                            nombre: $("#nombre").val(), cedula: $("#cedula").val(),
                            clave: $("#clave").val(), rol: $("#rol").val()
                        };
                        insertarModificarUsuario(rowData);
                        $("#dataTable").jqxDataTable('updateRow', editRow, rowData);
                    });

                    $("#dialog").on('close', function () {
                        // enable jqxDataTable.
                        $("#dataTable").jqxDataTable({disabled: false });
                    });
                    $("#dialog").jqxWindow({
                        resizable: false,
                        theme: 'darkblue',
                        position: { left: $("#dataTable").offset().left + 75, top: $("#dataTable").offset().top + 35 },
                        width: 300, height: 270,
                        autoOpen: false
                    });
                    $("#dialog").css('visibility', 'visible');
                },
                pagerButtonsCount: 8,
                toolbarHeight: 35,
                renderToolbar: function(toolBar)
                {
                    var toTheme = function (className) {
                        if (theme == "") return className;
                        return className + " " + className + "-" + theme;
                    }
                    // appends buttons to the status bar.
                    var container = $("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                    var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                    var addButton = $(buttonTemplate);
                    var editButton = $(buttonTemplate);
                    var deleteButton = $(buttonTemplate);
                    container.append(addButton);
                    container.append(editButton);
                    container.append(deleteButton);
                    toolBar.append(container);
                    addButton.jqxButton({cursor: "pointer", enableDefault: false,  height: 25, width: 25 });
                    addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                    addButton.jqxTooltip({ position: 'bottom', content: "Agregar"});
                    editButton.jqxButton({ cursor: "pointer", disabled: true, enableDefault: false,  height: 25, width: 25 });
                    editButton.find('div:first').addClass(toTheme('jqx-icon-edit'));
                    editButton.jqxTooltip({ position: 'bottom', content: "Editar"});
                    deleteButton.jqxButton({ cursor: "pointer", disabled: true, enableDefault: false,  height: 25, width: 25 });
                    deleteButton.find('div:first').addClass(toTheme('jqx-icon-delete'));
                    deleteButton.jqxTooltip({ position: 'bottom', content: "Eliminar"});
                    var updateButtons = function (action) {
                        switch (action) {
                            case "Select":
                                addButton.jqxButton({ disabled: false });
                                deleteButton.jqxButton({ disabled: false });
                                editButton.jqxButton({ disabled: false });
                                break;
                            case "Unselect":
                                addButton.jqxButton({ disabled: false });
                                deleteButton.jqxButton({ disabled: true });
                                editButton.jqxButton({ disabled: true });
                                break;
                        }
                    }
                    var rowIndex = null;
                    $("#dataTable").on('rowSelect', function (event) {
                        var args = event.args;
                        rowIndex = args.index;
                        updateButtons('Select');
                    });
                    $("#dataTable").on('rowUnselect', function (event) {
                        updateButtons('Unselect');
                    });
                    addButton.click(function () {
                        if (!addButton.jqxButton('disabled')) {
                            $("#dialog").jqxWindow('setTitle', "Insertar Usuario");
                            $("#dialog").jqxWindow('open');
                            $("#dataTable").jqxDataTable({ disabled: true });
                            $("#nombre").val("");
                            $("#cedula").val("");
                            $("#clave").val("");
                            $("#rol").val("");
                            
                            updateButtons('add');
                        }
                    });
                    editButton.click(function (event) {
                        if (!editButton.jqxButton('disabled')) {
                            var row = args.row;
                            $("#dialog").jqxWindow('setTitle', "Editar Usuario");
                            $("#dialog").jqxWindow('open');
                            $("#dataTable").jqxDataTable({editable: false,disabled: true });
                            $("#nombre").val(row.nombre);
                            $("#cedula").val(row.cedula);
                            $("#clave").val(row.clave);
                            $("#rol").val(row.rol);
                            
                            $("#dataTable").jqxDataTable('beginRowEdit', rowIndex);
                            updateButtons('edit');
                        }
                    });
                    deleteButton.click(function () {
                        if (!deleteButton.jqxButton('disabled')) {
                            $("#dataTable").jqxDataTable('deleteRow', rowIndex);
                            var row = args.row;
                            eliminarUsuario(row.cedula)
                            updateButtons('delete');
                        }
                    });
                },
                columns: [
                  { text: 'Nombre', dataField: 'nombre', width: 260 },
                  { text: 'C&eacute;dula', dataField: 'cedula', width: 260},
                  { text: 'Clave', dataField: 'clave', width: 260},
                  { text: 'Puesto', dataField: 'rol', width: 260}
              ]
            });
}

function alerta(dialog, evento){
      alertify.notify(dialog, evento, 5);
}