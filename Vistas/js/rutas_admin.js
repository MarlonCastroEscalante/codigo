var maps=[],
boundsArray=[],
modal=animatedModal({
    html:setInsertModalContent(),
    overlay:{closeOnClick:true},
    closeImg:'./Vistas/img/closebt.svg',
    title:'Agregar una nueva ruta <button class="btn btn-primary right" style="margin-right:6em" onclick="insertRoute()">Guardar</button>',
    style:{height:'100%',width:'100%',left:'0'},
    beforeOpenAnim:function(){}
}),
mapOptions= {mapTypeControl:false,streetViewControl:false,clickableIcons:false,mapTypeControlOptions:google.maps.MapTypeId.ROADMAP},
stops=[],infoWindows=[],horarios=[];

$.fn.serializeObject = function(){
    var o = {},a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {o[this.name] = [o[this.name]];}
            o[this.name].push(this.value || '');
        }
        else {o[this.name] = this.value || '';}
    });
    return o;
};

$(document).ready(function(){

    scheduleModal=$($.parseHTML('<div id="scheduleModal">'+
    '<header><h3>Insertar Horario</h3></header>'+
    '<form id="schedule-form">'+
    '<h4>Días</h4>'+
    '<div class="form-group-inline days">'+
    '<div class="form-control-group">'+
    '<label>De: </label>'+
    '<select class="form-control" name="from">'+
    '<option value="Lunes">Lunes</option>'+
    '<option value="Martes">Martes</option>'+
    '<option value="Miércoles">Miércoles</option>'+
    '<option value="Jueves">Jueves</option>'+
    '<option value="Viernes">Viernes</option>'+
    '<option value="Sábado">Sábado</option>'+
    '<option value="Domingo">Domingo</option>'+
    '</select>'+
    '</div>'+
    '<div class="form-control-group">'+
    '<label>a: </label>'+
    '<select class="form-control" name="to">'+
    '<option value="">(Ninguno)</option>'+
    '<option value="Lunes">Lunes</option>'+
    '<option value="Martes">Martes</option>'+
    '<option value="Miércoles">Miércoles</option>'+
    '<option value="Jueves">Jueves</option>'+
    '<option value="Viernes">Viernes</option>'+
    '<option value="Sábado">Sábado</option>'+
    '<option value="Domingo">Domingo</option>'+
    '</select>'+
    '</div>'+
    '</div>'+
    '<hr class="separator">'+
    '<table style="table-layout: fixed;" class="hours-table"><thead><tr style="text-align:left"><th>Horas</th><th></th></tr></thead>'+
    '<tbody><tr><td><input class="form-control" type="text" name="hours" placeholder="00:00 AM/PM"></td><td></td></tr></tbody></table>'+
    '<hr class="separator">'+
    '<a href="#" onclick="addHoursRow(\'#scheduleModal .hours-table\')" style="font-size:small;margin-left:1em;color:grey">+ Añadir más</a>'+
    '</form>'+
    '<div class="footer"><button onclick="addScheduleToList()" class="right btn btn-primary">Aceptar</button><a class="right closeModal btn btn-link">Cancelar</a></div>'+
    '</div>')).appendTo('body');

    $('#modal2').leanModal({closeButton:'.closeModal'});

    init();

    //Handler for dropdown lists
    $('[data-toggle]').click(function(){
        var ev=$(this);
        switch (ev.attr('data-toggle')) {
            case 'dropdown':
            dropdownToggle(ev);
            break;
        }
    });

    $('form').submit(function(e){e.preventDefault();})

    //Load schedule and stops
    var m=$('li.route-item');
    for (var i = 0; i < m.length; i++) {
        maps[i]=new google.maps.Map(document.getElementById('map-id-'+i),mapOptions);
        cargarRutas_Horarios(m[i],maps[i],i);
    }
});

function dropdownToggle(e){
    var t=$('#'+e.attr('data-target'));
    t.fadeToggle({duration:0,start:function(){t.offset({top:e.outerHeight()+e.offset().top,left:e.offset().left});}});
}
function init(){
    var input=document.getElementById('acm');
    mapOptions.center=new google.maps.LatLng({lat:0.0,lng:0.0});
    directionsService = new google.maps.DirectionsService();
    insertmap=new google.maps.Map(document.getElementById('insertmap'),{
        center: {lat: 10.935451, lng: -85.275913},
        zoom: 8,
        mapTypeControl:false,
        streetViewControl:false
    });

    insertmap.controls[google.maps.ControlPosition.TOP].push(input);

    autocomplete=new google.maps.places.Autocomplete(input,{componentRestrictions:{country:'cr'}});
    marker=new google.maps.Marker({map: insertmap});
    info=new google.maps.InfoWindow({
        content:'<div class="form-group">'+
        '<input class="form-control insert-stop" name="parada" placeholder="Lugar de parada" autofocus>'+
        '<button class="insert-stop btn btn-success" onclick="addStopToList(this)">Agregar</button>'+
        '</div>'
    });

    google.maps.event.addListener(marker,'click',function(){info.open(insertmap,marker);});
    google.maps.event.addListener(info,'closeclick',function(){marker.setMap(null);});
    google.maps.event.addListener(insertmap,'click',function(event){
        if(marker.getMap()===null || marker.getMap()===undefined){marker.setMap(insertmap);}
        marker.setPosition(event.latLng);
        info.open(insertmap,marker);
    });

    modal.settings.afterOpen=function(){google.maps.event.trigger(insertmap, 'resize');};
}

function addStopToList(e){
    var input=$(e).prev(),
    markerElement=new google.maps.Marker({map:insertmap,position:marker.getPosition()}),
    pos=markerElement.getPosition(),
    infoElement=new google.maps.InfoWindow({position:markerElement.getPosition()});

    google.maps.event.addListener(markerElement,'click',function(){infoElement.open(insertmap,markerElement);});

    //stops.push({nombre:input.val,latitud:pos.lat(),longitud:pos.lng()});
    stops.push({place:input.val(),marker:markerElement});
    infoWindows.push(infoElement);

    var index=infoWindows.length-1;

    $('#stops-list-insert')
    .append('<li class="list-group-item" data-index="'+index+'">'+input.val()+'</li>')
    .parent()
    .css('border','none')
    .find('p')
    .fadeOut();

    infoWindows[index].setContent(
        '<span style="margin:2em;vertical-align:middle">'+input.val()+'</span>'+
        '<button title="Eliminar" style="color:#d44f3a;background:transparent;border:none" onclick="deleteStop('+index+')"><span class="glyphicon glyphicon-trash"></span></button>'+
        '<button title="Editar" style="color:#EEA236;background:transparent;border:none;padding-left:6px" onclick="editStop('+index+',\''+input.val()+'\')"><span class="glyphicon glyphicon-pencil"></span></button>'
    );
    input.val('');
    info.close();
}
function saveStopChanges(index,e){
    var val=$(e).prev().val().trim();

    //validar insercion
    if(val===''){}

    infoWindows[index].setContent(
        '<span style="margin:2em;vertical-align:middle">'+val+'</span>'+
        '<button title="Eliminar" style="color:#d44f3a;background:transparent;border:none" onclick="deleteStop('+index+')"><span class="glyphicon glyphicon-trash"></span></button>'+
        '<button title="Editar" style="color:#EEA236;background:transparent;border:none;padding-left:6px" onclick="editStop('+index+',\''+val+'\')"><span class="glyphicon glyphicon-pencil"></span></button>'
    );

    //actualizar la lista
    $('#stops-list-insert').find('li[data-index='+index+']').text(val);
}
function deleteStop(index){
    //infoWindows[index].close();
    marker.setMap(null);
    stops[index].marker.setMap(null);
    //stops.splice(index,1);
    stops[index]=null;
    infoWindows[index]=null;
    //infoWindows.splice(index,1);

    //actualizar la lista
    $('#stops-list-insert').find('li[data-index='+index+']').fadeOut({duration:300,complete:function(){$(this).remove()}});
}
function editStop(index,value){infoWindows[index].setContent('<div class="form-group"><input class="form-control insert-stop" value="'+value+'" name="parada" placeholder="Lugar de parada" autofocus><button class="insert-stop btn btn-warning" onclick="saveStopChanges('+index+',this)">Guardar</button></div>');}

function collapseContent(e,i){
    var content=$($($(e).parent()).next());
    $('.route-content').slideUp();
    if(content.hasClass('slide')){content.removeClass('slide').slideUp();}
    else {
        content.addClass('slide').slideDown({duration:300,complete:function(){
            maps[i].fitBounds(boundsArray[i]);
            maps[i].setCenter(boundsArray[i].getCenter());
            google.maps.event.trigger(maps[i], 'resize');
        }});
    }
}

function addHoursRow(s){$(s).find('tbody').append('<tr><td><input class="form-control" type="text" name="hours" placeholder="00:00 AM/PM"></td><td><button onclick="$(this).parent().parent().remove()" style="border:none;color:#d44f3a;background-color:transparent" type="button"><span style="top:2px" class="glyphicon glyphicon-trash"></span></button></td></tr>');}
function addScheduleToList(){
    var schd=$('#schedule-form').serializeObject();
    horarios.push(schd);

    //update DOM
    var str1='',last_index=horarios.length-1;
    str1+='<div style="padding:0 2em" class="left"><h4>'+schd.from+((schd.to!=='')?' a '+schd.to:'')+
    '<button title="Eliminar" style="color:#d44f3a;background:transparent;border:none" onclick="deleteSchedule(this,'+last_index+')"><span class="glyphicon glyphicon-trash"></span></button>'+
    '<button title="Editar" style="color:#EEA236;background:transparent;border:none;padding-left:6px" onclick="editSchedule(this,\''+schd.from+'\',\''+schd.to+'\','+last_index+')"><span class="glyphicon glyphicon-pencil"></span></button>'+
    '<button title="Agregar" style="color:#aaa;background:transparent;border:none;padding-left:6px" onclick="AddNewRowSchedule(this,'+last_index+')"><span class="glyphicon glyphicon-plus"></span></button></h4>'+
    '<ul class="list-group">';

    if(typeof schd.hours === 'string'){
        schd.hours=[schd.hours];
        str1+='<li class="list-group-item">'+
        schd.hours[0]+
        '<button title="Editar" style="color:#EEA236;background:transparent;border:none;padding-left:6px" onclick="editScheduleRow(this,\''+schd.hours[0]+'\','+last_index+',0)"><span class="glyphicon glyphicon-pencil"></span></button>'+
        '</li>';
    }
    else{
        str1+='<li class="list-group-item">'+
        schd.hours[0]+
        '<button title="Editar" style="color:#EEA236;background:transparent;border:none;padding-left:6px" onclick="editScheduleRow(this,\''+schd.hours[0]+'\','+last_index+',0)"><span class="glyphicon glyphicon-pencil"></span></button>'+
        '</li>';
        for (var k = 1; k < schd.hours.length; k++) {
            str1+='<li class="list-group-item">'+
            schd.hours[k]+
            '<button title="Eliminar" style="color:#d44f3a;background:transparent;border:none" onclick="deleteScheduleRow(this,'+last_index+','+k+')"><span class="glyphicon glyphicon-trash"></span></button>'+
            '<button title="Editar" style="color:#EEA236;background:transparent;border:none;padding-left:6px" onclick="editScheduleRow(this,\''+schd.hours[k]+'\','+last_index+','+k+')"><span class="glyphicon glyphicon-pencil"></span></button>'+
            '</li>';
        }
    }
    str1+='</ul></div>';

    $('#schedule-list').append(str1).removeClass('empty');
    $('.closeModal').trigger('click');
}

function deleteScheduleRow(e,s,r){
    //delete from array
    horarios[s].hours[r]=null;

    //remove from DOM
    $(e).parent().fadeOut({duration:200,complete:function(){$(this).remove()}});
}
function editScheduleRow(e,v,s,i){$(e).parent().empty().append('<input type="text" class="form-control" value="'+v+'"><button title="Aceptar cambios" style="color:#5CB85C;background:transparent;border:none;padding-left:6px" onclick="saveScheduleRow(this,'+s+','+i+')"><span class="glyphicon glyphicon-ok"></span></button>');}
function saveScheduleRow(e,s,i){
    var row=$(e).parent(),
    val=$(e).prev().val();

    //update model
    horarios[s].hours[i]=val;
    console.log(horarios);
    //update DOM
    row.empty().append(
        val+
        ((i!==0)?'<button title="Eliminar" style="color:#d44f3a;background:transparent;border:none" onclick="deleteScheduleRow(this,'+s+','+i+')"><span class="glyphicon glyphicon-trash"></span></button>':'')+
        '<button title="Editar" style="color:#EEA236;background:transparent;border:none;padding-left:6px" onclick="editScheduleRow(this,\''+val+'\','+s+','+i+')"><span class="glyphicon glyphicon-pencil"></span></button>'
    );
}

function deleteSchedule(e,s){
    //delete from array
    horarios[s]=null;

    //remove from DOM
    $(e).parent().parent().fadeOut({duration:200,complete:function(){$(this).remove()}});
    console.log(horarios);
}
function editSchedule(e,f,t,s){
    $(e).parent().empty().append(
        '<div class="form-group-inline days">'+
        '<div class="form-control-group">'+
        '<label>De: </label>'+
        '<select type="text" class="form-control" name="from">'+
        '<option value="Lunes" '+((f==='Lunes')?'selected':'')+'>Lunes</option>'+
        '<option value="Martes" '+((f==='Martes')?'selected':'')+'>Martes</option>'+
        '<option value="Miércoles" '+((f==='Miércoles')?'selected':'')+'>Miércoles</option>'+
        '<option value="Jueves" '+((f==='Jueves')?'selected':'')+'>Jueves</option>'+
        '<option value="Viernes" '+((f==='Viernes')?'selected':'')+'>Viernes</option>'+
        '<option value="Sábado" '+((f==='Sábado')?'selected':'')+'>Sábado</option>'+
        '<option value="Domingo" '+((f==='Domingo')?'selected':'')+'>Domingo</option>'+
        '</select>'+
        '</div>'+
        '<div class="form-control-group">'+
        '<label>a: </label>'+
        '<select type="text" class="form-control" name="to">'+
        '<option value="" '+((t==='')?'selected':'')+'>(Ninguno)</option>'+
        '<option value="Lunes" '+((t==='Lunes')?'selected':'')+'>Lunes</option>'+
        '<option value="Martes" '+((t==='Martes')?'selected':'')+'>Martes</option>'+
        '<option value="Miércoles" '+((t==='Miércoles')?'selected':'')+'>Miércoles</option>'+
        '<option value="Jueves" '+((t==='Jueves')?'selected':'')+'>Jueves</option>'+
        '<option value="Viernes" '+((t==='Viernes')?'selected':'')+'>Viernes</option>'+
        '<option value="Sábado" '+((t==='Sábado')?'selected':'')+'>Sábado</option>'+
        '<option value="Domingo" '+((t==='Domingo')?'selected':'')+'>Domingo</option>'+
        '</select>'+
        '</div>'+
        '<button title="Aceptar cambios" style="color:#5CB85C;background:transparent;border:none;padding-left:6px" onclick="saveSchedule(this,'+s+')"><span class="glyphicon glyphicon-ok"></span></button>');
    }
function saveSchedule(e,s){
    var c=$(e).parent(),
    from_val=c.find('[name=from]').val(),
    to_val=c.find('[name=to]').val();

    //update model
    horarios[s].from=from_val;
    horarios[s].to=to_val;

    //update DOM
    c.parent().empty().append(
        '<h4>'+from_val+((to_val!=='')?' a '+to_val:'')+
        '<button title="Eliminar" style="color:#d44f3a;background:transparent;border:none" onclick="deleteSchedule(this,'+s+')"><span class="glyphicon glyphicon-trash"></span></button>'+
        '<button title="Editar" style="color:#EEA236;background:transparent;border:none;padding-left:6px" onclick="editSchedule(this,\''+from_val+'\',\''+to_val+'\','+s+')"><span class="glyphicon glyphicon-pencil"></span></button>'+
        '<button title="Agregar" style="color:#aaa;background:transparent;border:none;padding-left:6px" onclick="AddNewRowSchedule(this,'+s+')"><span class="glyphicon glyphicon-plus"></span></button></h4>'
    );
}
function AddNewRowSchedule(e,s){
    var i=horarios[s].hours.length;
    $(e)
    .parent().parent()
    .find('.list-group')
    .append(
        '<li class="list-group-item">'+
        '<input type="text" class="form-control">'+
        '<button title="Aceptar cambios" style="color:#5CB85C;background:transparent;border:none;padding-left:6px" onclick="saveScheduleRow(this,'+s+','+i+')"><span class="glyphicon glyphicon-ok"></span></button>'+
        '<button title="Cancelar" style="font-size:x-small;color:#aaa;background:transparent;border:none;padding-left:6px" onclick="cancelScheduleRow(this)"><span class="glyphicon glyphicon-remove"></span></button>'+
        '</li>'
    );
}
function cancelScheduleRow(e){$(e).parent().remove();}

function drawRouteAdmin(paradas,map,index){

    var waypoints=[],
    directionsDisplay = new google.maps.DirectionsRenderer({map:map});

    for(var i=0; i<paradas.length; i++){waypoints[i]={location:new google.maps.LatLng(paradas[i].latitud,paradas[i].longitud)};}
    var request = {
        origin: waypoints[0],
        destination: waypoints[waypoints.length-1],
        waypoints: waypoints,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    },bounds=null;
    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            setMapBounds(response.routes[0].bounds);
            directionsDisplay.setDirections(response);
        }
        else{alert('La ruta no es válida');}
    });
}
function setMapBounds(bounds) {boundsArray.push(bounds);}
function cargarRutas_Horarios(e,m,index){
    var l=$(e),
    sch=l.find('.schedule-list'),
    stp=l.find('.stops-list');

    $.ajax({
        url:'./Datos/datosRuta.php?handler=testGetAllRouteData',
        data:{id:l.attr('data-ruta-id'),tipo:true},
        type:'get',
        async:false,
        dataType:'json',
        success:function(response){
            var str1='',
            str2='<ul class="list-group">';
            for (var i = 0; i < response.horarios.length; i++) {
                str1+='<div style="padding:0 2em" class="left"><h4>'+response.horarios[i].comienzo+((response.horarios[i].final!==null)?' a '+response.horarios[i].final:'')+'</h4><ul class="list-group">';
                for (var k = 0; k < response.horarios[i].horas.length; k++) {
                    str1+='<li class="list-group-item">'+response.horarios[i].horas[k].hora+'</li>';
                }
                str1+='</ul></div>';
            }
            for (var i = 0; i < response.paradas.length; i++) {
                str2+='<li class="list-group-item">'+response.paradas[i].nombre+'</li>';
            }
            str2+='</ul>';
            sch.append(str1);
            stp.append(str2);

            drawRouteAdmin(response.paradas,m,index);
        }
    });
}

function insertRoute(){

    var paradas=[],
    info=$('#route-info-form').serializeObject();

    //sanitizeArray
    sanitizeArray(stops,null);

    for (var i = 0; i <horarios.length; i++) {sanitizeArray(horarios[i].hours,null);}
    for (var i = 0; i <stops.length; i++) {paradas[i]={lugar:stops[i].place,latitud:stops[i].marker.getPosition().lat(),longitud:stops[i].marker.getPosition().lng()}}

    $.ajax({
        url:'./Datos/datosRuta.php?handler=insertarRuta',
        type:'post',
        data:{
            info:info,
            paradas:paradas,
            horarios:horarios
        },
        dataType:'json',
        success:function(resp) {
            //put success alert here
            console.log(resp);
        }
    });
}

function sanitizeArray(array,value){for(var i=0;i<array.length;i++){if(array[i]===value){array.splice(i,1);i--;}}}
function setInsertModalContent(){
    return '<section class="row"><input id="acm" class="form-control" type="text" placeholder="Búsqueda rápida"><div class="map-controls"></div><div id="insertmap" class="lg-6 md-10 push-md-1 sm-12 xs-12"></div>'+
    '<div class="route-info lg-4 push-lg-1 md-6 push-md-3"><h3>Información de la ruta<hr class="separator"></h3>'+
    '<form id="route-info-form">'+
    '<div class="form-group-inline"><input class="form-control" name="salida" type="text" placeholder="Lugar de Salida"><input class="form-control" type="text" name="destino" placeholder="Lugar de Destino"></div>'+
    '<div class="form-group"><label for="ruta-salida" style="font-weight:600">Tarifa: </label><input name="tarifa" id="ruta-salida" class="form-control" type="text" placeholder="¢ Colones"></div>'+
    '</form><hr class="separator">'+
    '</div>'+
    '<div class="stops-insert lg-4 push-lg-1 md-6 push-md-3"><h3>Paradas<hr class="separator"></h3><div class="stops-list empty"><p><span class="glyphicon glyphicon-alert" style="font-size:40px;color:#ccc"></span>Seleccione un punto en el mapa para agregar una parada a la lista.<span></span></p><ul id="stops-list-insert" class="list-group empty"></ul></div></div>'+
    '<div class="schedule-insert lg-4 push-lg-1 md-6 push-md-3"><h3>Horarios<a id="modal2" href="#scheduleModal" style="font-size:small;margin-left:1em;color:grey;font-weight:100;padding: 8px 10px;border-radius: 3px;transition:background .3s">+ Añadir</a><hr class="separator"></h3><div id="schedule-list" style="overflow:hidden" class="empty"><p><span class="glyphicon glyphicon-alert" style="font-size:40px;color:#ccc"></span>Aún no se han creado horarios para esta ruta.'+
    '</p></div></section>';
}
