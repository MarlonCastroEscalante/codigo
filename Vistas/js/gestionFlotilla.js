 function insertarBus(bus){
    alert(bus);
        /*alertify.confirm('¿Seguro que desea continuar?','',function(){
        $.ajax({
            url:'../Datos/DatosBus.php',
            data: {accion:2,bus:bus},
            dataType: "json",
            type:'post',
            success:function(data) {
                cargarTabla();
                alerta("<b>La transaccion se realizó con exito</b>", "success");
            },
            error: function(){
            alerta("<b>Error al insertar</b>", "Error");
        }
        });

        }, function(){
        alertify.error('Cancelado')});*/
}

function eliminarBus(matricula){
    //alert(matricula);
    alertify.confirm('¿Seguro que desea Eliminar el bus?','',function(){
    $.ajax({
        url:'../Datos/DatosBus.php',
        data: {accion:3,matricula:matricula},
        type:'post',
        success:function(data) {
            cargarTabla();
            alerta("<b>Eliminado con exito</b>", "success");
        },
        error: function(){
            alerta("<b>Error al eliminar</b>", "Error");
        }
    });
 }, function(){
        alertify.error('Cancelado')});
}

function guardarImagen(){
    $('#jqxFileUpload').jqxFileUpload({multipleFilesUpload: false, rtl: true, browseTemplate: 'success', uploadTemplate: 'primary',  cancelTemplate: 'danger', width: 150, uploadUrl: 'guardarImagen.php', fileInputName: 'fileToUpload' });
}

 function cargarTabla(){
	         var url = '../Datos/DatosBus.php';
         var valores = ["Bueno","Malo", "En Reparación"]
         var tipo = ["Alquiler","Público"]
           var source =
            {
                dataType: "json",
                dataFields: [
                    { name: 'matricula', type: 'string' },
                    { name: 'estado', type: 'string' },
                    { name: 'tipo', type: 'string' },
                    { name: 'imagen', type: 'string' },
                    { name: 'capacidad', type: 'string' },
                    { name: 'descripcion', type: 'string' }
                ],
                data: {
                    accion:1
                },
                type:"POST",
                url: url,
                addRow: function (rowID, rowData, position, commit) {
                    commit(true);
                },
                updateRow: function (rowID, rowData, commit) {
                    commit(true);
                },
                deleteRow: function (rowID, commit) {
                    commit(true);
                },
                success:function(data){
                    alert(data);
                },
                error:function(){
                    alert("ERROR");
                }
            };
         
            var dataAdapter = new $.jqx.dataAdapter(source, {
                loadComplete: function () {
                }
            });
            $("#dataTable").jqxDataTable(
            {
                width: 1040,
                height: 380,
                source: dataAdapter,
                sortable: true,
                pageable: true,
                pageSize: 3,
                theme: 'darkblue',
                filterable: true,
                filterMode: 'simple',
                pagerButtonsCount: 5,
                showToolbar: true,
                ready: function()
                {
                    $("#matricula").jqxInput({disabled: false,width: 150, height: 30 });
                    $("#capacidad").jqxInput({width: 150, height: 30 });
                    $("#descripcion").jqxTextArea({ placeHolder: 'Escriba una descripción', height: 100, width: 155, minLength: 1});
                    $("#save").jqxButton({ height: 30, width: 80 });
                    $("#cancel").jqxButton({height: 30, width: 80 });
                    $("#cancel").mousedown(function () {
                        // close jqxWindow.
                        $("#dialog").jqxWindow('close');
                    });

                    $("#save").mousedown(function () {
                        // close jqxWindow.
                        $("#dialog").jqxWindow('close');
                        // update edited row.
                        var editRow = parseInt($("#dialog").attr('data-row'));
                        var rowData = {
                            matricula: $("#matricula").val(),capacidad: $("#capacidad").val(),
                            tipo: $("#tipo").val(),descripcion: $("#descripcion").val(),
                            imagen: $("#jqxFileUpload").val(),estado: $("#estado").val()};
                        insertarBus(rowData);
                        $("#dataTable").jqxDataTable('updateRow', editRow, rowData);
                    });

                    $("#dialog").on('close', function () {
                        // enable jqxDataTable.
                        $("#dataTable").jqxDataTable({disabled: false });
                        $("#matricula").empty();
                        $("#estado").val("");
                        $("#tipo").val("");
                        $("#descripcion").val("");
                        $("#capacidad").empty();
                    });
                    $("#dialog").jqxWindow({
                        resizable: false,
                        theme: 'darkblue',
                        position: { left: $("#dataTable").offset().left + 250, top: $("#dataTable").offset().top + 35 },
                        width: 550, height: 300,
                        autoOpen: false
                    });
                    $("#dialog").css('visibility', 'visible');
                },
                renderToolbar: function(toolBar)
                {
                  var toTheme = function (className) {
                        if (theme == "") return className;
                        return className + " " + className + "-" + theme;
                    }
                    var container = $("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
                    var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
                    var addButton = $(buttonTemplate);
                    var cancelButton = $(buttonTemplate);
                    var updateButton = $(buttonTemplate);
                    var editButton = $(buttonTemplate);
                    var deleteButton = $(buttonTemplate);
                    container.append(addButton);
                    container.append(editButton);
                    container.append(deleteButton);
                    toolBar.append(container);
                    addButton.jqxButton({cursor: "pointer", enableDefault: false,  height: 25, width: 25 });
                    addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
                    addButton.jqxTooltip({ position: 'bottom', content: "Nuevo"});
                    editButton.jqxButton({ cursor: "pointer", disabled: true, enableDefault: false,  height: 25, width: 25 });
                    editButton.find('div:first').addClass(toTheme('jqx-icon-edit'));
                    editButton.jqxTooltip({ position: 'bottom', content: "Editar"});
                    deleteButton.jqxButton({ cursor: "pointer", disabled: true, enableDefault: false,  height: 25, width: 25 });
                    deleteButton.find('div:first').addClass(toTheme('jqx-icon-delete'));
                    deleteButton.jqxTooltip({ position: 'bottom', content: "Eliminar"});
                    var updateButtons = function (action) {
                        switch (action) {
                            case "Select":
                                addButton.jqxButton({ disabled: false });
                                deleteButton.jqxButton({ disabled: false });
                                editButton.jqxButton({ disabled: false });
                                break;
                            case "Unselect":
                                addButton.jqxButton({ disabled: false });
                                deleteButton.jqxButton({ disabled: true });
                                editButton.jqxButton({ disabled: true });
                                break;
                        }
                    }
                    var rowIndex = null;
                    $("#dataTable").on('rowSelect', function (event) {
                        var args = event.args;
                        rowIndex = args.index;
                        updateButtons('Select');
                    });
                    $("#dataTable").on('rowUnselect', function (event) {
                        updateButtons('Unselect');
                    });
                    addButton.click(function (event) {
                        if (!addButton.jqxButton('disabled')) {
                            // add new empty row.
                            $("#estado").jqxComboBox({animationType: 'slide', source: valores,selectedIndex: 0, width: 150, height: 30});
                            $("#tipo").jqxComboBox({animationType: 'slide', source: tipo,selectedIndex: 0, width: 150, height: 30});
                            $("#dialog").jqxWindow('setTitle', "Nuevo Bus");
                            $("#dialog").jqxWindow('open');
                            $("#dataTable").jqxDataTable({ disabled: true });
                        }
                    });
                    editButton.click(function (event) {
                        if (!editButton.jqxButton('disabled')) {
                            var row = args.row;
                            $("#matricula").jqxInput({disabled: true,width: 150, height: 30 });
                            $("#estado").jqxComboBox({animationType: 'slide', source: valores,selectedIndex: 0, width: 150, height: 30});
                            $("#tipo").jqxComboBox({animationType: 'slide', source: tipo,selectedIndex: 0, width: 150, height: 30});
                            $("#matricula").val(row.matricula);
                            $("#estado").val(row.estado);
                            $("#tipo").val(row.tipo);
                            $("#descripcion").val(row.descripcion);
                            $("#capacidad").val(row.capacidad);
                            $("#imagen").val(row.imagen);
                            $("#dialog").jqxWindow('setTitle', "Editar Bus");
                            $("#dialog").jqxWindow('open');
                            //cargarBusesAlquiler('modificar');
                            //$("#dataTable").jqxDataTable('beginRowEdit', rowIndex);
                            updateButtons('edit');
                        }
                    });
                    deleteButton.click(function () {
                        if (!deleteButton.jqxButton('disabled')) {
                            var row = args.row;
                            eliminarBus(row.matricula);
                            //$("#dataTable").jqxDataTable('deleteRow', rowIndex);
                            updateButtons('delete');
                        }
                    });
                },
                columns: [
                      {
                          text: 'Imagen', align: 'center', dataField: 'matricula', width: 150, height: 1500,
                          cellsRenderer: function (row, column, value, dataField) {
                              var image = "<div style='margin: 5px; margin-bottom: 3px;'>";
                              var imgurl = 'img/' + dataField.imagen;
                              var img = '<img width="130" height="60" style="display: block;" src="' + imgurl + '"/>';
                              image += img;
                              image += "</div>";
                              return image;
                          }
                      },
                      {
                          text: 'Detalles', align: 'center', dataField: 'descripcion',
                          cellsRenderer: function (row, column, value, dataField) {
                              var container2 = '<div style="width: 100%; height: 100%;">'
                              var leftcolumn = '<div style="float: left; width: 40%;">';
                              var rightcolumn = '<div style="float: left; width: 60%;">';
                      
                              var tipo = "<div style='margin: 10px;'><b>Tipo:</b> " + dataField.tipo + "</div>";
                              var estado = "<div style='margin: 10px;'><b>Estado:</b> " + dataField.estado + "</div>";
                            
                              leftcolumn += tipo;
                              leftcolumn += estado;
                              leftcolumn += "</div>";

                              var capacidad = "<div style='margin: 10px;'><b>Capacidad:</b> " + dataField.capacidad + "</div>";
                              var descripcion = "<div style='margin: 10px;'><b>Descripci&oacute;n:</b> " + dataField.descripcion + "</div>";

                              rightcolumn += capacidad;
                              rightcolumn += descripcion;
                              rightcolumn += "</div>";

                              container2 += leftcolumn;
                              container2 += rightcolumn;
                              container2 += "</div>";
                              return container2;
                          }
                      }
                ]
            });
}

function alerta(dialog, evento){
      alertify.notify(dialog, evento, 5);
}