
var search_filter=0;

function toggleCollapse(e,t) {
    if(!$(e).hasClass('animate')){$(e).addClass('animate');}
    else{$(e).removeClass('animate');}
    $(t).slideToggle(400);
}

function setFilter(val,sel){
    search_filter=val;
    $(sel).find('.glyphicon:first').removeClass().addClass('glyphicon '+((val===0)?'glyphicon-road':'glyphicon-map-marker'));
    $('#filter-dropdown').fadeOut(0);

}

function autoCompleteSearch(e){
    var rl=$('#iwr').css('width',$('#buscar-ruta').outerWidth()+'px'),
        val=$(e).val();
    if(val!=='' && val!==null){
        $.ajax({
            url:'../Datos/datosRuta.php?handler=testGetRutas',
            data:{search:val,filter:search_filter},
            type:'get',
            dataType:'json',
            success:function(response) {
                rl.empty();
                var str='';
                if(search_filter===0){
                    for (var i = 0, len=response.length; i < len; i++) {str+='<li class="list-group-item" data-ruta-id="'+response[i].id+'" data-ruta-salida="'+response[i].salida+'" data-ruta-destino="'+response[i].destino+'" data-tarifa="'+response[i].tarifa+'" onclick="cargarRutas_Horarios(this)">De <b>'+response[i].salida+'</b> a <b>'+response[i].destino+'</b></li>';}
                    rl.append(str);
                    rl.offset({top:($(e).offset().top + $(e).outerHeight()),left:$(e).offset().left});
                }
                else{
                    for(var i = 0, len=response.length; i < len; i++) {
                        str+='<li class="list-group-item" data-ruta-id="'+response[i].id+'" data-ruta-salida="'+response[i].salida+'" data-ruta-destino="'+response[i].destino+'" data-tarifa="'+response[i].tarifa+'" onclick="cargarRutas_Horarios(this)"><span style="float:left;width:100%">De <b>'+response[i].salida+'</b> a <b>'+response[i].destino+'</b></span><span style="color:#888;font-size:small;float:left"><b>Para en: </b>'+response[i].paradas[0].nombre;
                        for (var j = 1; j < response[i].paradas.length; j++) {str+=', '+response[i].paradas[j].nombre;}
                        str+='</li>';
                    }
                    rl.append(str);
                    rl.offset({top:($(e).offset().top + $(e).outerHeight()),left:$(e).offset().left});
                }
            }
        });
    }
}

function convertToCRC(val){
    var res=0;
    $.ajax({
        url:'../libs/currencyconverter.php?handler=colonesADolares',
        data:{value:val},
        type:'post',
        dataType:'json',
        success:function(response) {res=response;}
    });
    return res;
}

function generatePaypalForm(amount,origen,destino){
    return ('<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">'+
        '<input type="hidden" name="notify_url" value="../PaypalResponseHandler.php">'+
        '<input type="hidden" name="cmd" value="_xclick">'+
        '<input type="hidden" name="rm" value="1">'+
        '<input type="hidden" name="business" value="varogonz95@gmail.com">'+
        '<input type="hidden" name="lc" value="US">'+
        '<input type="hidden" name="button_subtype" value="services">'+
        '<input type="hidden" name="item_name" value="Tiquete de '+origen+' a '+destino+'">'+
        '<input type="hidden" name="amount" value="'+convertToCRC(amount)+'">'+
        '<input type="hidden" name="currency_code" value="USD">'+
        '<input type="hidden" name="button_subtype" value="services">'+
        '<input type="hidden" name="no_note" value="1">'+
        '<input type="hidden" name="no_shipping" value="1">'+
        '<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_SM.gif:NonHosted">'+
        '<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynow_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">'+
        '<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">'+
    '</form>');
}

function cargarRutas_Horarios(e){
    var l=$(e);
    $('#route-title').text(l.attr('data-ruta-salida')+' - '+l.attr('data-ruta-destino')).append('<span class="right"> Tarifa: ¢ '+l.attr('data-tarifa')+generatePaypalForm(Number(l.attr('data-tarifa')),l.attr('data-ruta-salida'),l.attr('data-ruta-destino')))+'</span>';
    $('#buscar-ruta').val(l.attr('data-ruta-salida')+' a '+l.attr('data-ruta-destino'));
    $('#schedule-list').empty();
    $('#stops-list').empty();
    $.ajax({
        url:'../Datos/datosRuta.php?handler=testGetAllRouteData',
        data:{id:l.attr('data-ruta-id'),tipo:true},
        type:'get',
        dataType:'json',
        success:function(response){
            var str1='',
                str2='<ul class="list-group">';
            for (var i = 0; i < response.horarios.length; i++) {
                    str1+='<div style="padding:0 2em" class="left"><h4>'+response.horarios[i].comienzo+((response.horarios[i].final!==null)?' a '+response.horarios[i].final:'')+'</h4><ul class="list-group">';
                    for (var k = 0; k < response.horarios[i].horas.length; k++) {
                        str1+='<li class="list-group-item">'+response.horarios[i].horas[k].hora+'</li>';
                    }
                    str1+='</ul></div>';
            }
            for (var i = 0; i < response.paradas.length; i++) {
                str2+='<li class="list-group-item">'+response.paradas[i].nombre+'</li>';
            }
            str2+='</ul>';
            $('#iwr li').remove();
            $('#schedule-list').append(str1);
            $('#stops-list').append(str2);

            drawRoute(response.paradas);
        }
    });
}

function drawRoute(paradas){
    var waypoints=[],
    directionsService = new google.maps.DirectionsService();
    for(i=0; i<paradas.length; i++){waypoints[i]={location:new google.maps.LatLng(paradas[i].latitud,paradas[i].longitud)};}
    var request = {
        origin: waypoints[0],
        destination: waypoints[waypoints.length-1],
        waypoints: waypoints,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    },
    bounds=null;

    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            bounds = response.routes[0].bounds;
            directionsDisplay.setDirections(response);
        }
        else{alert('failed to get directions');}
    });

    $('#map-container').removeClass('hidden');
    $('#map-container p').addClass('hidden');
    $('#map-canvas').fadeIn({duration:300,complete:function(){
        google.maps.event.trigger(map, 'resize');
        map.fitBounds(bounds);
        map.setCenter(bounds.getCenter());
    }});
}

function initialize() {
    //geocoder = new google.maps.Geocoder(),
    mapOptions = {zoom:10,mapTypeControl:false,streetViewControl:false,clickableIcons:false,mapTypeControlOptions:google.maps.MapTypeId.ROADMAP},
    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions),
    directionsDisplay = new google.maps.DirectionsRenderer({map:map});
}
