function isEmpty(valor){
    return (valor.trim()==='' || !valor)?true:false;
}

function isInteger(valor){
    return !isNaN(Number(valor));
}

function isValidEmail(mail){
    return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(mail)?true:false;
}

function validarLetras(valor){
    return /^[a-zA-Z0-9 èàùìòÈÀÒÙÌéáúíóÉÁÚÍÓëäüïöËÄÜÏÖêâûîôÊÂÛÎÔç'-]*$/.test(valor)?true:false;
}

function soloNumeros(e)

{

    // capturamos la tecla pulsada

    var teclaPulsada=window.event ? window.event.keyCode:e.which;

    // capturamos el contenido del input

    var valor=document.getElementById("jqxInput").value;



    if(valor.length<10)

    {

        // 13 = tecla enter

        // Si el usuario pulsa la tecla enter o el punto y no hay ningun otro

        // punto

        if(teclaPulsada==13 || teclaPulsada==08)

        {

            return true;

        }



        // devolvemos true o false dependiendo de si es numerico o no

        return /\d/.test(String.fromCharCode(teclaPulsada));

    }else{

        return false;

    }

}
