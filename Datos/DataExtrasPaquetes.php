<?php

include_once 'Conexion.php';
include_once 'Validaciones.php';
/*include_once '../Modelo/Encomienda.php';
include_once '../Modelo/Paquete.php';*/

if(isset($_GET['handler'])){
    switch ($_GET['handler']) {
        case 'handler':
        //method();
        break;
    }
}

if(isset($_POST['accion'])){
    $accion = $_POST['accion'];
}
switch ($accion) {
    case 1:
    listarCategorias();
    break;
    case 2:
    listarDimensiones();
    break;
}



function listarCategorias(){
    $data=array();

    $con = conectar();
    if(!($result=mysqli_query($con,"call spIMECCategoriaPaquete(4,null,null)"))){
        desconectar($con);
        return false;
    }

    while($fila = mysqli_fetch_array($result,MYSQLI_ASSOC)) {$data[]=$fila;}

    echo json_encode($data);
}

function buscarCategoria($id){

    $data=array();

    $con = conectar();
    if(!($result=mysqli_query($con,"call spIMECCategoriaPaquete(5,$id,null)"))){
        desconectar($con);
        return false;
    }

    while($fila = mysqli_fetch_array($result,MYSQLI_ASSOC)) {$data[]=$fila;}

    echo json_encode($data);
}

function eliminarCategoria($id){
    $con = conectar();
    if(!mysqli_query($con,"call spIMECCategoriaPaquete(3,$id,null)")){
        desconectar($con);
        return false;
    }
    return true;
}

function modificarCategoria($id,$categoria){
    //validar
    if(!validarRequerido($categoria)){return false;}
    if(!validarLetras($categoria)){return false;}

    $con = conectar();

    if(!mysqli_query($con,"call spIMECCategoriaPaquete(2,$id,'$categoria')")){
        desconectar($con);
        return false;
    }
    return true;
}

//Inserta y retorna el id del paquete insertado
function insertarCategoriaId($categoria){
    //validar
    if(!validarRequerido($categoria)){return false;}
    if(!validarLetras($categoria)){return false;}

    $con = conectar();
    if(!$result=mysqli_query($con,"call spIMECCategoriaPaquete(1,null,'$categoria')")){
        desconectar($con);
        return false;
    }

    echo mysqli_fetch_array($result,MYSQLI_ASSOC)['id'];
}

function insertarCategoria($categoria){
    //validar
    if(!validarRequerido($categoria)){return false;}
    if(!validarLetras($categoria)){return false;}

    $con = conectar();
    if(!mysqli_query($con,"call spIMECCategoriaPaquete(0,null,'$categoria')")){
        desconectar($con);
        return false;
    }
    return true;
}

function listarDimensiones(){
    $data=array();

    $con = conectar();
    if(!($result=mysqli_query($con,"call spIMECDimensionesPaquete(4,null,null,null,null,null)"))){
        desconectar($con);
        return false;
    }

    while($fila = mysqli_fetch_array($result,MYSQLI_ASSOC)) {$data[]=$fila;}

    echo json_encode($data);
}

function buscarDimensiones($id){

    $data=array();

    $con = conectar();
    if(!($result=mysqli_query($con,"call spIMECDimensionesPaquete(5,$id,null,null,null,null)"))){
        desconectar($con);
        return false;
    }

    while($fila = mysqli_fetch_array($result,MYSQLI_ASSOC)) {$data[]=$fila;}

    echo json_encode($data);
}

function eliminarDimension($id){
    $con = conectar();
    if(!mysqli_query($con,"call spIMECDimensionesPaquete(3,$id,null,null,null,null)")){
        desconectar($con);
        return false;
    }
    return true;
}

function modificarDimension($id,$dimension,$alto,$largo,$ancho){
    //validar
    if(!validarRequerido($dimension)){return false;}
    if(!validarLetras($dimension)){return false;}
    if(!is_numeric($alto)){return false;}
    if(!is_numeric($largo)){return false;}
    if(!is_numeric($ancho)){return false;}

    $con = conectar();

    if(!mysqli_query($con,"call spIMECDimensionesPaquete(2,$id,'$dimension',$alto,$largo,$ancho)")){
        desconectar($con);
        return false;
    }
    return true;
}

//Inserta y retorna el id del paquete insertado
function insertarDimensionId($dimension,$alto,$largo,$ancho){
    //validar
    if(!validarRequerido($dimension)){return false;}
    if(!validarLetras($dimension)){return false;}
    if(!is_numeric($alto)){return false;}
    if(!is_numeric($largo)){return false;}
    if(!is_numeric($ancho)){return false;}

    $con = conectar();
    if(!$result=mysqli_query($con,"call spIMECDimensionesPaquete(1,null,'$dimension',$alto,$largo,$ancho)")){
        desconectar($con);
        return false;
    }

    echo mysqli_fetch_array($result,MYSQLI_ASSOC)['id'];
}

function insertarDimension($dimension,$alto,$largo,$ancho){
    //validar
    if(!validarRequerido($dimension)){return false;}
    if(!validarLetras($dimension)){return false;}
    if(!is_numeric($alto)){return false;}
    if(!is_numeric($largo)){return false;}
    if(!is_numeric($ancho)){return false;}

    $con = conectar();
    if(!mysqli_query($con,"call spIMECDimensionesPaquete(0,null,'$dimension',$alto,$largo,$ancho)")){
        desconectar($con);
        return false;
    }
    return true;
}

?>
