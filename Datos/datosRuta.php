<?php
include_once 'Conexion.php';
include_once 'Validaciones.php';
include_once 'DatosParada.php';
include_once 'DatosHorario.php';

if(isset($_POST['accion'])){
    switch ($_POST['accion']) {
        case 1:
        buscarRutas();
        break;
    }
}

if(isset($_GET['handler'])){
    switch ($_GET['handler']) {
        case 'testGetRutas':
        buscarRutas($_GET['search'],intval($_GET['filter']));
        break;
        case 'testGetHorarios':
        testGetHorarios($_GET['id']);
        break;
        case 'testGetAllRouteData':
        testGetAllRouteData($_GET['id'],$_GET['tipo']);
        break;
        case 'testGetAllRoutes':
        testGetRutas(true);
        break;
        case 'insertarRuta':
        insertarRutaTodo($_POST);
        break;
    }
}

function buscarRutas($busqueda,$f){

    $rutas=array();
    if($f===0){
        $link = conectar();
        if(!($rutas_query=$link->query("call spTestBuscarRuta(0,'$busqueda%');"))) {return false;}
        desconectar($link);
        while($rutas_row= mysqli_fetch_array($rutas_query,MYSQLI_ASSOC)){array_push($rutas, $rutas_row);}
    }
    else{
        $link = conectar();
        if(!($paradas_query=$link->query("select * from testParadas where nombre like '$busqueda%';"))) {return false;}
        desconectar($link);
        while($paradas_row= mysqli_fetch_array($paradas_query,MYSQLI_ASSOC)){
            $link = conectar();
            if(!($rutas_query=$link->query("select r.* from testRutas r, testParadasRutas pr where r.id=pr.idRuta and pr.idParada={$paradas_row['id']};"))) {return false;}
            desconectar($link);
            while($rutas_row= mysqli_fetch_array($rutas_query,MYSQLI_ASSOC)){
                if(!empty($rutas)){
                    if(value_exist($rutas,'id',$rutas_row['id'])){
                            $rutas[array_search_key($rutas,'id',$rutas_row['id'])]['paradas'][]=$paradas_row;
                    }
                    else{
                        $rutas_row['paradas'][]=$paradas_row;
                        $rutas[]=$rutas_row;
                    }
                }
                else {
                    $rutas_row['paradas'][]=$paradas_row;
                    $rutas[]=$rutas_row;
                }
            }
        }
    }
    echo json_encode($rutas);
}

function array_search_key($array,$_key,$_value){
    $out=0;
    for ($i=0,$len=count($array); $i < $len; $i++) {
        foreach ($array as $key => $value) {
            if($key===$_key && $value===$_value){$out=$i; break;}
        }
    }
    return $out;
}

function mostrarRutas(){
    $link=conectar();
    if(!($result=$link->query('CALL paMostrarRutas()'))) {
        echo "Falló CALL: (" . $link->errno . ") " . $link->error;
    }
    desconectar($link);
    $rutas = array();
    if($result!=NULL){
        if(mysqli_num_rows($result)>0){
            while($fila=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $ruta = new Ruta();
                $ruta->setCodigo($fila['codigo']);
                $ruta->setOrigen($fila['origen']);
                $ruta->setDestino($fila['destino']);
                $ruta->setTiempoEstimado($fila['tiempoEstimado']);
                $paradas = mostrarParadas($fila['codigo']);
                $ruta->setParadas($paradas);
                array_push($rutas, $ruta);
            }
        }
    }
    return $rutas;
}

function mostrarParadas($codigoRuta){
    $link = conectar();
    if(!($result=$link->query('CALL paMostrarParadas("'.$codigoRuta.'")'))) {
        echo "Falló CALL: (" . $link->errno . ") " . $link->error;
    }
    desconectar($link);
    $paradas = array();
    while($fila= mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $parada = new Parada();
        $parada->setCodigo($fila['codigo']);
        $parada->setNombre($fila['nombre']);
        $parada->setPrecio($fila['precio']);
        array_push($paradas, $parada);
    }
    return $paradas;
}

function mostrarHorario($codigoRuta){
    $link = conectar();
    if(!($result=$link->query('CALL paMostrarHorario("'.$codigoRuta.'"")'))) {
        echo "Falló CALL: (" . $link->errno . ") " . $link->error;
    }
    desconectar($link);
    $horas = array();
    while ($fila = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
        array_push($horas, $fila['hora']);
    }
    return $horas;
}

//############################## Codigo Alvaro ####################################
function testGetRutas($json=false,$pag=0){
    $con = conectar();
    $rutas = array();
    if(!($result=mysqli_query($con,"select * from testRutas".(($pag>0)?" limit $pag":";")))){
        desconectar($con);
        return false;
    }
    while ($fila = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
        $rutas[]= $fila;
    }
    if ($json)
        echo json_encode($rutas);
    else
        return $rutas;
}

function testRutasHoras($horario,$tipo){
    $con = conectar();
    $horas = array();
    if(!($result=mysqli_query($con,"call spTestRutaHoras($horario,$tipo);"))){
        echo "Fallo en la conexion a la base de datos.<br>Error: " . mysqli_connect_error();
    }
    desconectar($con);
    while ($fila = mysqli_fetch_array($result,MYSQLI_ASSOC)) { array_push($horas, $fila); }
    return $horas;
}

function testGetHorarios($ruta,$tipo){
    $con = conectar();
    $horarios=array();
    if(!($result=mysqli_query($con,"select * from testRutasHorarios where idRuta=$ruta;"))){
        echo "Fallo en la conexion a la base de datos.<br>Error: " . mysqli_connect_error();
    }

    while ($fila = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
        $horas=testRutasHoras($fila['id'],$tipo);
        $fila['horas']=$horas;
        array_push($horarios, $fila);
    }

    return $horarios;
}

function testGetParadas($ruta){
    $con = conectar();
    $paradas = array();
    if(!($result=mysqli_query($con,"call spTestParadasRuta($ruta);"))){
        echo "Fallo en la conexion a la base de datos.<br>Error: " . mysqli_connect_error();
    }
    desconectar($con);
    while ($fila = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
        array_push($paradas, $fila);
    }
    return $paradas;
}

function testGetAllRouteData($ruta,$tipo,$json=true){
    if(!$json)
        return array('horarios'=>testGetHorarios($ruta,$tipo), 'paradas'=>testGetParadas($ruta));
    else
        echo json_encode(array('horarios'=>testGetHorarios($ruta,$tipo), 'paradas'=>testGetParadas($ruta)));
}

function mostrarRutasHorarios($codigoRuta){
    $con = conectar();
    if(!($result=mysqli_query($con,"call spPruebaRutasHorarios($codigoRuta);"))){
        echo "Fallo en la conexion a la base de datos.<br>Error: " . mysqli_connect_error();
    }
    desconectar($con);
    $rutasHorarios = array();
    while ($fila = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
        array_push($rutasHorarios, $fila);
    }
    return $rutasHorarios;
}

function value_exist($array,$key,$value){
    $out=false;
    for ($i=0,$len=count($array); $i < $len; $i++) {
        if($array[$i][$key]===$value){$out=true;break;}
    }
    return $out;
}

function insertarRutaTodo($data){
    $ruta=insertarRuta($data['info']['salida'],$data['info']['destino'],'ninguno',$data['info']['tarifa']);
    if($ruta!==false){
        //insertar las paradas
        for($i=0,$len=count($data['paradas']);$i<$len;$i++){if(!insertarParadaRuta($ruta,$data['paradas'][$i]['lugar'],$data['paradas'][$i]['latitud'],$data['paradas'][$i]['longitud'])){echo json_encode(array('sucess' => false,'msg'=>'FUCK 1.' ));exit();}}

        //insertar los horarios
        for($i=0,$len=count($data['horarios']);$i<$len;$i++){if(!insertarHorarioRuta($ruta,$data['horarios'][$i]['from'],$data['horarios'][$i]['to'],$data['horarios'][$i]['hours'])){echo json_encode(array('sucess' => false,'msg'=>'FUCK 2.' ));exit();}}
    }

    echo json_encode(array('sucess' => true,'msg'=>'FUCK YEAH' ));
}
//##################################################################################

//############################## Codigo Ginnette ####################################
function insertarRuta($salida,$destino,$comentario,$tarifa){
   $link = conectar();
   $error = true;
   if((!validarRequerido($salida)) || (!validarRequerido($destino)) || (!validarRequerido($tarifa)) || (!validarFloat($tarifa)) || (!validarLetras($salida)) || (!validarLetras($destino))) {
    $error = false;
    }else{
      if(!($result=mysqli_query($link,"call paInsertarRuta('$salida','$destino','$comentario',$tarifa)"))) {
         return false;
      }
    }
  desconectar($link);
  return mysqli_fetch_array($result,MYSQLI_ASSOC)['id'];
}

function eliminarRuta($id){
    $link = conectar();
    $error = true;
    if(!validarRequerido($id)){
      $error = false;
    }else{
      if(!($result=$link->query('CALL paEliminarRuta("'.$id.'")'))) {
         return "Falló CALL: (" . $link->errno . ") " . $link->error;
      }
    }
  desconectar($link);
  return $error;
}

function modificarRuta($id, $salida,$destino,$comentario,$tarifa){
   $link = conectar();
   $error = true;
  if((!validarRequerido($salida)) || (!validarRequerido($destino)) || (!validarRequerido($tarifa)) || (!validarFloat($tarifa)) || (!validarLetras($salida)) || (!validarLetras($destino))) {
    $error = false;
    }else{
      if(!($result=$link->query('CALL paModificarRuta("'.$id.'","'.$salida.'","'.$destino.'","'.$comentario.'","'.$tarifa.'")'))) {
         return "Falló CALL: (" . $link->errno . ") " . $link->error;
      }
    }
  desconectar($link);
  return $error;
}
//####################################################################################
