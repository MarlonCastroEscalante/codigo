<?php
 function validarRequerido($valor){
    if(trim($valor)===''){
       return false;
    }else{
       return true;
    }
 }
 function validarEntero($valor, $opciones=null){
    if(filter_var($valor, FILTER_VALIDATE_INT, $opciones) === FALSE){
       return false;
    }else{
       return true;
    }
 }
 function validarEmail($valor){
    if(filter_var($valor, FILTER_VALIDATE_EMAIL) === FALSE){
       return false;
    }else{
       return true;
    }
 }

 function validarFloat($valor){
    if(filter_var($valor, FILTER_VALIDATE_FLOAT) === FALSE){
       return false;
    }else{
       return true;
    }
 }


 function validarLetras($valor){
   if (preg_match("/^([a-zA-Z ]+)$/", $valor)) {
   //"^[a-zA-Z0-9\-_]{3,20}$"
      return true;
   } else {
      return false;
   }
}
?>
