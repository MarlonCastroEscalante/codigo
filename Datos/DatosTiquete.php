<?php
include_once 'Conexion.php';

if(isset($_GET['handler'])){
    switch ($_GET['handler']) {
        case 'getTiqueteInfo':
        getTiqueteInfo();
        break;
    }
}

function getTiqueteInfo(){
    if(($cambio=calcularVuelto(intval($_POST['efectivo']),intval($_POST['total'])))!==false){
        session_start();
        echo json_encode(array(
            'user'=>$_SESSION['user']['nombre'],
            'fecha'=>date('d/m/Y h:i:s a'),
            'tarifa'=>'',
            'total'=>$_POST['total'],
            'efectivo'=>$_POST['efectivo'],
            'cambio'=>calcularVuelto(intval($_POST['efectivo']),intval($_POST['total'])),
            'errors'=>false
        ));
        exit(0);
    }
    echo json_encode(array('errors'=>true));
    exit(0);
}

function insertarTiquete($codRuta, $costo, $estado, $codSucursal){
   $link = conectar();

  if(!($result=$link->query('CALL paInsertarTiquete("'.$codRuta.'","'.$costo.'","'.$estado.'","'.$codSucursal.'")'))) {
          desconectar($link);
         return false;
      }
      else{
          desconectar($link);
      	  return true;
      }
}



function calcularVuelto($pago, $costo){

    //validar
    // 1- El pago debe ser igual o mayor al costo
    return ($pago>=$costo)?$pago-$costo:false;
}
?>
