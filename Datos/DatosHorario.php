<?php

function insertarHorarioRuta($ruta,$desde,$hasta,$horas /*<-array de horas*/){

    //insertar el horario
    $con = conectar();
    $horario=mysqli_query($con,"call spInsertarHorario($ruta,'$desde',".(($hasta==='')?null:"'$hasta'").")");
    desconectar($con);

    //Si el horario no es FALSE
    if($horario!==false){

        //Crear id del horario a partir del resultado del insert
        $idHorario=mysqli_fetch_array($horario,MYSQLI_ASSOC)['id'];

        //entonces iterar las horas
        for ($i=0,$len=count($horas); $i <$len ; $i++) {
            //insertar la hora del indice $i en cada iteracion
            $con = conectar();
            if(!mysqli_query($con,"insert into testRutaHoras values(null,$idHorario,'{$horas[$i]}')")){
                echo json_encode(['query'=>"insert into testRutaHoras values(null,$idHorario,'{$horas[$i]}')"]);
                exit();
            }
            desconectar($con);
        }
    }
    //Si el horario es false, terminar operacion y retornar false
    else {return false;}

    //Retornar true, todo salió bien
    return true;
}

?>
