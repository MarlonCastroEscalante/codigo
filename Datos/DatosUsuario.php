<?php
include_once 'Conexion.php';
include_once 'Validaciones.php';
include_once '../Modelo/Usuario.php';

$accion=0;
if(isset($_POST['accion'])){
    $accion = $_POST['accion'];
}
switch ($accion) {
    case 1:
        $cedula = $_POST['ced'];
        $contraseña = $_POST['passw'];
        verificarUsuario($cedula, $contraseña);
        break;
    case 2:
        mostrarUsuarios();
        break;
    case 3:
        $usuario = $_POST['usuario'];
        insertarModificarUsuario($usuario);
        break;
    case 4:
        $cedula = $_POST['cedula'];
        eliminarUsuario($cedula);
        break;
    //     break;
}

function verificarUsuario($cedula,$contrasena){

    $link = conectar();
    //if((!validarRequerido($cedula)) || (!validarRequerido($contrasena)) || (!validarEntero($cedula)){
    //return false;
    //}else{
    if(!($result=$link->query('CALL paVerificarUsuario("'.$contrasena.'","'.$cedula.'")'))){
        echo "Falló CALL: (" . $link->errno . ") " . $link->error;
    }
    $usuario = mysqli_fetch_assoc($result);
    desconectar($link);

    if (isset($usuario['cedula'])){
        //return $usuario;
        session_start();
        $_SESSION['user']=$usuario;
        switch ($usuario['rol']){
            case 'administrador': header('Location: ../Vistas/administrador.php'); break;
            case 'encomienda': header('Location: ../Vistas/encosistema.php'); break;
            case 'ventanilla': header('Location: ../Vistas/ventanilla.php'); break;
        }
    }else{
        return false;
    }
    //}
}

function mostrarUsuarios(){
    $link = conectar();

    if(!($result=$link->query('CALL paMostrarUsuarios()'))) {
        echo "Falló CALL: (" . $link->errno . ") " . $link->error;
    }
    desconectar($link);
    $usuarios=array();
    if($result!=NULL){
      if(mysqli_num_rows($result)>0){
        while($fila=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $usuarios[] = $fila;
        }
      }
    }
    echo json_encode($usuarios);
}


function insertarModificarUsuario($usuario){
    $link = conectar();
    session_start();
    $sucursal = $_SESSION['user']['sucursal'];

    /*if(!validarRequerido($usuario['nombre'])){return false;}
    if(!validarLetras($usuario['nombre'])){return false;}
    if(!validarRequerido($usuario['cedula'])){return false;}
    if(!is_numeric($usuario['cedula'])){return false;}
    if(!validarRequerido($usuario['rol'])){return false;}
    if(!validarLetras($usuario['rol'])){return false;}
    if(!validarRequerido($usuario['clave'])){return false;}*/

        if(!($result=$link->query('CALL paInsertarModificarUsuario("'.$usuario['cedula'].'","'.$usuario['nombre'].'","'.$usuario['clave'].'","'.$usuario['rol'].'","'.$sucursal.'")'))) {
            desconectar($link);
            return false;
        }
    //}
    desconectar($link);
    return true;
}

function eliminarUsuario($ced){
    $link = conectar();

    if(!($result=$link->query('CALL paEliminarUsuario("'.$ced.'")'))) {
        return "Falló CALL: (" . $link->errno . ") " . $link->error;
    }
    desconectar($link);
    return true;
}

/*function modificarUsuario($usuario){
    $link = conectar();

    if(!($result=$link->query('CALL paModificarUsuario("'.$usuario['cedula'].'","'.$usuario['nombre'].'","'.$usuario['rol'].'","'.$usuario['clave'].'","'.$sucursal.'")'))) {
            desconectar($link);
            return false;
        }
    desconectar($link);
    return true;
}*/

//Creación de método de cargar datos para modificación de usuario
function obtenerUsuario($nombre, $puesto){
    $link = conectar();

    if(!($result=$link->query('CALL paBuscarUsuario("'.$nombre.'", "'.$puesto.'")'))) {
        echo "Falló CALL: (" . $link->errno . ") " . $link->error;
    }
    desconectar($link);
    $fila = mysqli_fetch_assoc($result);
    $usuario = new Usuario();
    $usuario->setCedula($fila['cedula']);
    $usuario->setNombre($fila['nombre']);
    $usuario->setRol($fila['rol']);
    $usuario->setClave($fila['clave']);
    return $usuario;
}
?>
