<?php
include_once 'Conexion.php';
include_once 'Validaciones.php';
include_once '../Modelo/Bus.php';


$accion=0;
if(isset($_POST['accion'])){
    $accion = $_POST['accion'];
}
switch ($accion) {
    case 1:
        mostrarBus();
        break;
      case 2:
        $bus = $_POST['bus'];
        insertarBus($bus);
        break;
      case 3:
        $matricula = $_POST['matricula'];
        eliminarBus($matricula);
        break;
}

function eliminarBus($matricula){
    $con=conectar();
    if(!($result2=$con->query('CALL paEliminarBus("'.$matricula.'")'))){
        desconectar($con);
        return json_encode(false);
    }
    return json_encode(true);
} 

//Preguntar parametros de busqueda
function buscarBus($capacidad){
  $link = conectar();

  if(!($result=$link->query('CALL paMostrarBus("'.$capacidad.'")'))) {
          echo "Falló CALL: (" . $link->errno . ") " . $link->error;
      }
  desconectar($link);
   $buses = array();   
   if($result!=NULL){
      if(mysqli_num_rows($result)>0){
        while($fila= mysqli_fetch_array($result)){
          $bus = new Bus();
          $bus->setMatricula($fila['matricula']);
          $bus->setCapacidad($fila['capacidad']);
          $bus->setEstado($fila['estado']);
          $bus->setTipo($fila['tipo']);
          $bus->setImagen($fila['imagen']);
          array_push($buses, $bus);
        }
      }
    }
  return $buses;
}

//Muestra solo los buses que complen con la condicion que el tipo sea Alquiler
function mostrarBus(){
  $link = conectar();

  if(!($result=$link->query('CALL paMostrarBusesAlquiler()'))) {
          echo "Falló CALL: (" . $link->errno . ") " . $link->error;
      }
  desconectar($link);
   $buses = array();   
   if($result!=NULL){
      if(mysqli_num_rows($result)>0){
        while($fila= mysqli_fetch_array($result)){
          $buses[]=$fila;
        }
      }
    }
  echo json_encode($buses);
}


function insertarBus($bus){
   $link = conectar();
    if(!validarRequerido($bus['matricula'])){return json_encode(false);}
    if(!validarLetras($bus['estado'])){return json_encode(false);}
    if(!validarRequerido($bus['capacidad'])){return json_encode(false);}
    if(!validarRequerido($bus['estado'])){return json_encode(false);}
    if(!validarLetras($bus['tipo'])){return json_encode(false);}
    if(!validarRequerido($bus['tipo'])){return json_encode(false);}
    if(!is_numeric($bus['capacidad'])){return json_encode(false);}



      if(!($result=$link->query('CALL paInsertarBus("'.$bus['matricula'].'","'.$bus['capacidad'].'","'.$bus['estado'].'","'.$bus['tipo'].'","'.$bus['imagen'].'","'.$bus['descripcion'].'")'))) {
        desconectar($con);
        return json_encode(false);
      }

  desconectar($link);
  return json_encode(true);
}
?>