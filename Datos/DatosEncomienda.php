<?php
include_once 'Conexion.php';
include_once 'Validaciones.php';
include_once '../Modelo/Encomienda.php';
include_once '../Modelo/Paquete.php';

$accion=0;
if(isset($_POST['accion'])){
    $accion = $_POST['accion'];
}
switch ($accion) {
    case 1:
    mostrarEncomiendas();
    break;
    case 2:
        insertarEncomienda($_POST['encomienda']);
      break;
}

if(isset($_GET['handler'])){
    switch ($_GET['handler']) {
        case 'getEncomiendaPaquetes':
        getEncomiendaPaquetes($_GET['cod']);
        break;

    }
}

//Recibe un valor entero de 0 a 2, indicando el tipo de filtro.
// 0 listar todas las encomiendas. No es necesario pasar este parametro.
// 1 listar las encomiendas pendientes.
// 2 listar las encomiendas recibidas.
function mostrarEncomiendas($filtro=0){

  $link = conectar();
  if(!($result=$link->query("CALL paMostrarEncomiendas($filtro)"))) {
          echo "Falló CALL: (" . $link->errno . ") " . $link->error;
      }
  desconectar($link);

  $encomiendas = array();

  while($fila= mysqli_fetch_array($result)){
      /*$encomienda = new Encomienda();
      $encomienda->setNumeroGuia($fila['numeroGuia']);
      $encomienda->setFecha($fila['fecha']);
      //Pendiente: calcular precio
      $paquetes = getPaquetes($fila['numeroGuia']);
      $encomienda->setPaquetes($paquetes);
      array_push($encomiendas, $encomienda);*/
      $encomiendas[]=$fila;
  }
    echo json_encode($encomiendas);
  }

  function getPaquetes($numeroGuia){
    $link = conectar();
  if(!($result=$link->query('CALL paMostrarPaquetes("'.$numeroGuia.'")'))) {
          echo "Falló CALL: (" . $link->errno . ") " . $link->error;
      }
  desconectar($link);
  $paquetes=array();
  while($fila=mysqli_fetch_array($result)){
    $paquete = new Paquete();
    $paquete->setPeso($fila['peso']);
    $paquete->setCategoria($fila['categoria']);
    $paquete->setContenido($fila['contenido']);
    array_push($paquetes, $paquete);
  }
  return $paquetes;
  }


//Recibe un array asociativo ('clave'=>valor) de la forma:
//  $array(
//        'atributo_encomienda' => valor...,
//        'paquetes'=>array(
//                          array('atributo_paquete' => valor...),
//                          array('atributo_paquete' => valor...),
//                      )
//  );
//Ej:
// $data = array(
//      'usuario_emisor' =>'123456789',
//      'sucursal_salida' =>1,
//      'sucursal_destino' =>2,
//      'cedula_remitente' =>'702460892',
//      'nombre_remitente' =>'Pamela',
//      'telefono_remitente' =>'87654321',
//      'cedula_destinatario' =>'402280426',
//      'nombre_destinatario' =>'Alvaro',
//      'telefono_destinatario' =>'12345678',
//      'tarifa' =>1500,
//      'comentario' =>'tratar con amor cuidado',
//      'paquetes'=>array(0=>array('peso' => 123),1=>array('peso' => 123))
//  );
function insertarEncomienda($datos){ //inserta la encomienda y los paquetes

    $data = json_decode($datos);
    //validar
    if(!validarRequerido($data['usuario_emisor'])){return false;}
    if(!is_numeric($data['usuario_emisor'])){return false;}
    if(!validarRequerido($data['sucursal_salida'])){return false;}
    if(!is_numeric($data['sucursal_salida'])){return false;}
    if(!validarRequerido($data['sucursal_destino'])){return false;}
    if(!is_numeric($data['destino'])){return false;}
    if(!validarRequerido($data['cedula_remitente'])){return false;}
    if(!is_numeric($data['cedula_remitente'])){return false;}
    if(!validarLetras($data['nombre_remitente'])){return false;}
    if(!validarRequerido($data['nombre_remitente'])){return false;}
    if(!validarRequerido($data['cedula_destinatario'])){return false;}
    if(!is_numeric($data['cedula_destinatario'])){return false;}
    if(!validarRequerido($data['nombre_destinatario'])){return false;}
    if(!validarLetras($data['nombre_destinatario'])){return false;}

    $len=count($data['paquetes']);

    //validar array de paquetes
    for($i=0;$i<$len;$i++){
        if(!validarRequerido($data['paquetes'][$i]['descripcion'])){return false;}
        if(!validarLetras($data['paquetes'][$i]['descripcion'])){return false;}
        if(!validarRequerido($data['paquetes'][$i]['peso'])){return false;}
        if(!is_numeric($data['paquetes'][$i]['peso'])){return false;}
    }

    $con = conectar();
    if(!($encomienda=mysqli_query($con,"call paInsertarEncomiendas('{$data['usuario_emisor']}',{$data['sucursal_salida']},{$data['sucursal_destino']},'{$data['cedula_remitente']}','{$data['nombre_remitente']}','{$data['telefono_remitente']}','{$data['cedula_destinatario']}','{$data['nombre_destinatario']}','{$data['telefono_destinatario']}',{$data['tarifa']},'{$data['comentario']}')"))){
        desconectar($con);
        return false;
    }

    $id=mysqli_fetch_array($encomienda,MYSQLI_ASSOC);

    for($i=0;$i<$len;$i++){
        $con = conectar();
        if(!mysqli_query($con,"call spInsertarPaquetesEncomienda({$id['id']},{$data['paquetes'][$i]['dimension']},{$data['paquetes'][$i]['categoria']},{$data['paquetes'][$i]['peso']},'{$data['paquetes'][$i]['descripcion']}');")){
            desconectar($con);
            return false;
        }
        desconectar($con);
    }
    return true;
}

//Creación de método de obtener comentario de encomienda
function comentarioEncomienda($cod,$cmt){
    //validar
    if(!validarRequerido($cmt)){return false;}

    $con = conectar();
    if(($result=mysqli_query($con,"call spComentarioEncomienda($cod,'$cmt')"))){return true;}
    desconectar($con);
    return false;
}

//Creación de método buscar encomienda
//Preguntar parametros
function buscarEncomienda($numGuia){
  $link = conectar();

  if(!($result=$link->query('CALL paBuscarEncomienda("'.$numGuia.'")'))) {
          echo "Falló CALL: (" . $link->errno . ") " . $link->error;
      }
  desconectar($link);

  return mysqli_fetch_array($result,MYSQLI_ASSOC);
}

//Recibe el id de la encomienda (cod) y la cedula del empleado que la recibe (cedula)
function recibirEncomienda($cod,$cedula){
    //validar
    if(!validarRequerido($cedula)){return false;}
    if(!is_numeric($cedula)){return false;}

    $con = conectar();
    if(!mysqli_query($con,"call spRecibirEncomienda($cod,'$cedula')")){
        desconectar($con);
        return false;
    }
    return true;
}

function actualizarEstado($numeroGuia, $estado){
  //Recibe un array de numeros de guia para actualizar a un mismo estado
  $con = conectar();
  foreach($numerosGuia as &$numGuia){
    if(!($result=$link->query('CALL paActualizarEstadoEncomienda('.$numGuia.','.$estado.')'))) {
      return "Falló CALL: (" . $link->errno . ") " . $link->error;
    }
  }
    desconectar($con);
    return true;
}

function getEncomiendaPaquetes($cod){
    $d=array();
    $con = conectar();
    if(!($result=mysqli_query($con,"call paMostrarPaquetes($cod);"))){echo "Fallo en la conexion a la base de datos.<br>Error: " . mysqli_connect_error();}
    desconectar($con);

    while($fila = mysqli_fetch_array($result,MYSQLI_ASSOC)) {$d[]=$fila;}

    echo json_encode(array('encomienda'=>buscarEncomienda($cod),'paquetes'=>$d));
}
