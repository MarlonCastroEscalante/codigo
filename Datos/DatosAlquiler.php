<?php
include_once 'Conexion.php';
include_once 'Validaciones.php';
include_once '../Modelo/Bus.php';


$accion=0;
if(isset($_POST['accion'])){
    $accion = $_POST['accion'];
}
switch ($accion) {
    case 1:
      mostrarAlquiler();
      break;
    case 2:
      $codigo = $_POST['codigo'];
      eliminarAlquiler($codigo);
      break;
    case 3:
      $alquiler = $_POST['alquiler'];
      insertarAlquiler($alquiler);
      break;
   /* case 4:
      $codigoAlquiler = $_POST['codigoAlquiler'];
      obtenerBusesDeAlquiler($codigoAlquiler);
      break;*/
}

if(isset($_GET['handler'])){
    switch ($_GET['handler']) {
        case 'getBusesAlquiler':
        obtenerBusesDeAlquiler($_GET['cod']);
        break;
    }
}


function mostrarAlquiler(){
  $link = conectar();

  if(!($result=$link->query('CALL paMostrarAlquileres()'))) {
          echo "Falló CALL: (" . $link->errno . ") " . $link->error;
      }
  desconectar($link);
   $alquileres = array();   
   if($result!=NULL){
      if(mysqli_num_rows($result)>0){
        while($fila= mysqli_fetch_array($result)){
          $alquileres[]=$fila;
        }
      }
    }
  echo json_encode($alquileres);
}

function obtenerBusesDeAlquiler($cod){
    $d=array();
    $con = conectar();
    if(!($result=mysqli_query($con,"call paBuscarBusPorAlquiler($cod);"))){echo "Fallo en la conexion a la base de datos.<br>Error: " . mysqli_connect_error();}
    desconectar($con);

    while($fila = mysqli_fetch_array($result,MYSQLI_ASSOC)) {$bus[]=$fila;}

    echo json_encode(array('buses'=>$bus));
}

    function insertarAlquiler($data){ //insertar alquiler
    //validar
    /*if(!validarRequerido($data['nombre_cliente'])){return false;}
    if(!validarLetras($data['nombre_cliente'])){return false;}
    if(!validarRequerido($data['cedula_cliente'])){return false;}
    if(!validarRequerido($data['telefono_cliente'])){return false;}
    if(!validarLetras($data['fecha_alquiler'])){return false;}
    if(!validarRequerido($data['fecha_final'])){return false;}
    if(!validarRequerido($data['monto'])){return false;}

    //$len=count($data['buses']);

    //validar array de buses
    for($i=0;$i<$len;$i++){
        if(!validarRequerido($data['paquetes'][$i]['descripcion'])){return false;}
        if(!validarLetras($data['paquetes'][$i]['descripcion'])){return false;}
        if(!validarRequerido($data['paquetes'][$i]['peso'])){return false;}
        if(!is_numeric($data['paquetes'][$i]['peso'])){return false;}
    }*/
    list($dia, $mes,$año) = split('[/.-]',$data['fecha_alquiler']);
    $fechaInicial = $año."-".$mes."-".$dia;
    list($dia2, $mes2,$año2) = split('[/.-]',$data['fecha_final']);
    $fechaFinal = $año2."-".$mes2."-".$dia2;
    
    $con = conectar();
    if(!($result=$con->query('CALL paInsertarAlquiler("'.$data['nombre_cliente'].'","'.$data['cedula_cliente'].'","'.$data['telefono_cliente'].'","'.$data['telefono_cliente2'].'","'.$fechaInicial.'","'.$fechaFinal.'","'.$data['monto'].'")'))) {
         desconectar($con);
        return false;
      }
    $num = $data['cont'];
    $id=mysqli_fetch_array($result,MYSQLI_ASSOC);

    for($i=0;$i<=$num;$i++){
        $con = conectar();
        //echo($data['buses'][$i][0].' num '.$i);
        if(!($result2=$con->query('call paLigarBusAlquiler("'.$id['codigo'].'","'.$data['buses'][$i][0].'")'))){
          desconectar($con);
          return json_encode(false);
        }
      }
    desconectar($con);
    return json_encode(true);
    }

  //function modificarAlquiler($data){ //insertar alquiler
    //validar
    /*
    if(!validarLetras($data['fecha_alquiler'])){return false;}
    if(!validarRequerido($data['fecha_final'])){return false;}
    if(!validarRequerido($data['monto'])){return false;}

    //$len=count($data['buses']);

    //validar array de buses
    for($i=0;$i<$len;$i++){
        if(!validarRequerido($data['paquetes'][$i]['descripcion'])){return false;}
        if(!validarLetras($data['paquetes'][$i]['descripcion'])){return false;}
        if(!validarRequerido($data['paquetes'][$i]['peso'])){return false;}
        if(!is_numeric($data['paquetes'][$i]['peso'])){return false;}
    }*/
   /* list($dia, $mes,$año) = split('[/.-]',$data['fecha_alquiler']);
    $fechaInicial = $año."-".$mes."-".$dia;
    list($dia2, $mes2,$año2) = split('[/.-]',$data['fecha_final']);
    $fechaFinal = $año2."-".$mes2."-".$dia2;
    
    $con = conectar();
    if(!($result=$con->query('CALL paModificarAlquiler("'.$fechaInicial.'","'.$fechaFinal.'","'.$data['monto'].'")'))) {
         desconectar($con);
        return false;
      }
    $num = $data['cont'];
    $id=mysqli_fetch_array($result,MYSQLI_ASSOC);

        if(!($result2=$con->query('call paEliminarBusAlquiler("'.$data['codigo'].'")'))){
          desconectar($con);
          return false;
        }
    for($i=0;$i<=$num;$i++){
        $con = conectar();
        echo($data['buses'][$i][0].' num '.$i);
        if(!($result2=$con->query('call paLigarBusAlquiler("'.$data['codigo'].'","'.$data['buses'][$i][0].'")'))){
          desconectar($con);
          return false;
        }
      }*/
    desconectar($con);
    return true;
    }

function eliminarAlquiler($id){
    $con=conectar();
    $pago ='Pago';
    if(!($result2=$con->query('update alquiler_bus set pagoefectuado="'.$pago.'" where codigo="'.$id.'";'))){
        desconectar($con);
        return json_encode(false);
    }
    return json_encode(true);
}

?>