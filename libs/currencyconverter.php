<?php

if(isset($_GET['handler'])){
    switch ($_GET['handler']) {
        case 'colonesADolares':
        colonesADolares(floatval($_POST['value']));
        break;
    }
}

    function consumirPayPalWS(){
        $service='http://indicadoreseconomicos.bccr.fi.cr/indicadoreseconomicos/WebServices/wsIndicadoresEconomicos.asmx?WSDL';
        $date=date('d/m/Y');
        $params = array(
            'tcIndicador'=>'318',
            'tcFechaInicio'=>$date,
            'tcFechaFinal'=>$date,
            'tcNombre'=>'Alvaro Gonzalez Quiros',
            'tnSubNiveles'=>'N'
        );
        $client = new SoapClient($service,$params);
        return floatval(simplexml_load_string($client->ObtenerIndicadoresEconomicosXML($params)->ObtenerIndicadoresEconomicosXMLResult)->INGC011_CAT_INDICADORECONOMIC[0]->NUM_VALOR);
    }

    function colonesADolares($value){
        echo json_encode(number_format($value/consumirPayPalWS(),2,'.',''));
    }
