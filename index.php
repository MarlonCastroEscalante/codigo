<?php include './Controladoras/UsuarioController.php'; redirect('p'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{Nombre de la Aplicacion} | Inicio</title>

    <link rel="stylesheet" href="./Vistas/css/global.css">
    <link rel="stylesheet" href="./Vistas/css/inicio.css">
    <link rel="stylesheet" href="./Vistas/css/navbar.css">
    <link rel="stylesheet" href="./Vistas/css/glyphicons.css">
    <link rel="stylesheet" href="./Vistas/js/slider/slider.css">
</head>

<body>

    <?php include './Vistas/partials/navbar2.php'; ?>

    <header class="lg-10 push-lg-1 md-10 push-md-1 sm-12 push-sm-0">
        <ul id="slider">
            <li><img src="./Vistas/img/slide1.jpg" alt=""></li>
            <li><img src="./Vistas/img/slide7.png" alt=""></li>
            <li><img src="./Vistas/img/slide8.jpg" alt=""></li>
        </ul>
    </header>

            <section>
                <div class="articles row">
                    <div class="pusher lg-1 push-lg-1 md-12 push-md-0 sm-12 xs-12"></div>
                    <article class="lg-3 md-6 sm-8 xs-12 push-md-3 push-sm-2 push-xs-0">
                        <h1 class="header">Rutas y Horarios</h1>
                        <p class="text-body">Acceda al sitio para ver el trayecto de las rutas y sus horarios. También podrá realizar la compra de su tiquete, sin filas ni atrasos.</p>
                        <a class="btn btn-primary btn-link" href="./Vistas/rutas.php">Ir</a>
                    </article>
                    <article class="lg-3 md-6 sm-8 xs-12 push-md-3 push-sm-2 push-xs-0">
                        <h1 class="header">Rastreo de Encomiendas</h1>
                        <p class="text-body">Con su numero de envío, podrá saber el estado de sus encomiendas.</p>
                        <a class="btn btn-primary btn-link" href="./Vistas/tracking.php">Ir</a>
                    </article>
                    <article class="lg-3 md-6 sm-8 xs-12 push-md-3 push-sm-2 push-xs-0">
                        <h1 class="header">Alquiler de Transporte</h1>
                        <p class="text-body">Desde aquí podrá realizar el alquiler de alguno de nuestros transportes para las actividades que desee. Acceda al sitio y póngase en contacto con nosotros.</p>
                        <a class="btn btn-primary btn-link" href="./Vistas/busesCliente.php">Ir</a>
                    </article>
                    <a class="btn btn-primary btn-link" href="./prueba.php">Ir</a>
                </div>
            </section>

            <footer>
                <hr class="separator">
                <p>&copy; 2016 Company, Inc.</p>
            </footer>

            <script src="./Vistas/js/jquery.google.js"></script>
            <script src="./Vistas/js/slider/slider.js"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('#slider').bxSlider({auto:true});

                    $('[ui-action="collapse"]').click(function(){
                        var t=$('#'+$(this).attr('ui-target'));
                        if(!t.hasClass('collapsed')){
                            t.addClass('collapsed');
                            $('.navbar').css('height','90');
                        }
                        else {
                            t.removeClass('collapsed');
                            $('.navbar').css('height','45');
                        }
                    });
                });
            </script>
        </body>
        </html>
