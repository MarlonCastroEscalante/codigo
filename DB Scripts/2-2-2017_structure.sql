-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: ucrgrupo3
-- Source Schemata: ucrgrupo3
-- Created: Wed Feb  1 23:13:33 2017
-- Workbench Version: 6.3.6
-- ----------------------------------------------------------------------------

-- ----------------------------------------------------------------------------
-- Schema ucrgrupo3
-- ----------------------------------------------------------------------------
DROP SCHEMA IF EXISTS `ucrgrupo3` ;
CREATE SCHEMA IF NOT EXISTS `ucrgrupo3` ;
	
use ucrgrupo3;

SET FOREIGN_KEY_CHECKS = 0;
-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.Bus
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`Bus` (
  `matricula` VARCHAR(15) NOT NULL,
  `capacidad` INT(11) NULL DEFAULT NULL,
  `estado` VARCHAR(20) NULL DEFAULT NULL,
  `tipo` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`matricula`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.CategoriaPaquete
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`CategoriaPaquete` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.DimensionesPaquete
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`DimensionesPaquete` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(100) NULL DEFAULT NULL,
  `alto` DOUBLE NULL DEFAULT NULL,
  `largo` DOUBLE NULL DEFAULT NULL,
  `ancho` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.Encomienda
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`Encomienda` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `usuario_emisor` varchar(20) NULL DEFAULT NULL,
  `usuario_receptor` varchar(20) NULL DEFAULT NULL,
  `sucursal_salida` INT(11) NULL DEFAULT NULL,
  `sucursal_destino` INT(11) NULL DEFAULT NULL,
  `cedula_remitente` VARCHAR(9) NULL DEFAULT NULL,
  `nombre_remitente` VARCHAR(255) NULL DEFAULT NULL,
  `telefono_remitente` VARCHAR(8) NULL DEFAULT NULL,
  `cedula_destinatario` VARCHAR(9) NULL DEFAULT NULL,
  `nombre_destinatario` VARCHAR(255) NULL DEFAULT NULL,
  `telefono_destinatario` VARCHAR(8) NULL DEFAULT NULL,
  `fecha_enviado` DATE NULL DEFAULT NULL,
  `fecha_recibido` DATE NULL DEFAULT NULL,
  `tarifa` DOUBLE NULL DEFAULT NULL,
  `comentario` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
constraint fk_sucursal_salida foreign key (sucursal_salida) references Sucursal(codigo),
constraint fk_sucursal_destino foreign key (sucursal_destino) references Sucursal(codigo),
constraint fk_usuario_receptor foreign key (usuario_receptor) references Usuario(cedula),
constraint fk_usuario_emisor foreign key (usuario_emisor) references Usuario(cedula)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.Paquete
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`Paquete` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `dimensiones` INT(11) NULL DEFAULT NULL,
  `categoria` INT(11) NULL DEFAULT NULL,
  `descripcion` VARCHAR(255) NULL DEFAULT NULL,
  `peso` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
constraint fk_categoria_paquete foreign key (categoria) references CategoriaPaquete(id),
constraint fk_dimensiones_paquete foreign key (dimensiones) references DimensionesPaquete(id))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.PaqueteEncomienda
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`PaqueteEncomienda` (
  `encomienda` INT(11) NOT NULL DEFAULT '0',
  `paquete` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`encomienda`, `paquete`),
constraint fk_encomienda foreign key (encomienda) references Encomienda(id),
constraint fk_paquete foreign key (paquete) references Paquete(id)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.Sucursal
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`Sucursal` (
  `codigo` INT(11) NOT NULL AUTO_INCREMENT,
  `cedAdmi` varchar(20) NULL DEFAULT NULL,
  `ubicacion` VARCHAR(100) NULL DEFAULT NULL,
  `estado` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`codigo`),
constraint fk_admin foreign key (cedAdmi) references Usuario(cedula)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.Tiquete
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`Tiquete` (
  `codigo` INT(11) NOT NULL AUTO_INCREMENT,
  `codigoRuta` INT(11) NULL DEFAULT NULL,
  `codigoSucursal` INT(11) NULL DEFAULT NULL,
  `costo` FLOAT NULL DEFAULT NULL,
  `fecha` DATE NULL DEFAULT NULL,
  `estado` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`codigo`),
constraint fk_ruta_tiquete foreign key (codigoRuta) references testRutas(id),
constraint fk_sucursal_tiquete foreign key (codigoSucursal) references Sucursal(codigo)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.Usuario
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`Usuario` (
  `cedula` VARCHAR(20) NOT NULL,
  `nombre` VARCHAR(100) NULL DEFAULT NULL,
  `clave` VARCHAR(30) NULL DEFAULT NULL,
  `rol` VARCHAR(30) NULL DEFAULT NULL,
  PRIMARY KEY (`cedula`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.alquiler_bus
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`alquiler_bus` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `matricula_bus` VARCHAR(15) NULL DEFAULT NULL,
  `nombre_cliente` VARCHAR(255) NULL DEFAULT NULL,
  `cedula_cliente` VARCHAR(9) NULL DEFAULT NULL,
  `telefono_cliente` VARCHAR(8) NULL DEFAULT NULL,
  `telefono_cliente2` VARCHAR(8) NULL DEFAULT NULL,
  `fecha_alquiler` DATE NULL DEFAULT NULL,
  `fecha_final` DATE NULL DEFAULT NULL,
  `monto` DOUBLE NULL DEFAULT NULL,
  `pagoefectuado` TINYINT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
constraint fk_bus_matricula foreign key (matricula_bus) references Bus(matricula)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.imagen
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`imagen` (
  `nombre` VARCHAR(50) NULL DEFAULT NULL,
  `num` INT(11) NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.testParadas
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`testParadas` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NULL DEFAULT NULL,
  `latitud` DOUBLE NULL DEFAULT NULL,
  `longitud` DOUBLE NULL DEFAULT NULL,
  `prioridad` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.testParadasRutas
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`testParadasRutas` (
  `idRuta` INT(11) NOT NULL,
  `idParada` INT(11) NOT NULL,
  `tiempoEstimado` TIME NULL DEFAULT NULL,
  `tarifa` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idRuta`, `idParada`),
  CONSTRAINT `fk_parada` FOREIGN KEY (`idParada`) REFERENCES `ucrgrupo3`.`testParadas` (`id`),
  CONSTRAINT `fk_ruta` FOREIGN KEY (`idRuta`) REFERENCES `ucrgrupo3`.`testRutas` (`id`)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.testRutaHoras
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`testRutaHoras` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `idHorario` INT(11) NULL DEFAULT NULL,
  `hora` TIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_horario`
    FOREIGN KEY (`idHorario`)
    REFERENCES `ucrgrupo3`.`testRutasHorarios` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.testRutas
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`testRutas` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `salida` VARCHAR(50) NULL DEFAULT NULL,
  `destino` VARCHAR(50) NULL DEFAULT NULL,
  `comentario` VARCHAR(255) NULL DEFAULT NULL,
  `tarifa` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Table ucrgrupo3.testRutasHorarios
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `ucrgrupo3`.`testRutasHorarios` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `idRuta` INT(11) NULL DEFAULT NULL,
  `comienzo` VARCHAR(15) NULL DEFAULT NULL,
  `final` VARCHAR(15) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_ruta_horario`
    FOREIGN KEY (`idRuta`)
    REFERENCES `ucrgrupo3`.`testRutas` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paBuscarEncomienda
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paBuscarEncomienda`(IN cod int)
BEGIN 
	SELECT * FROM Encomienda  where id = cod;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paBuscarRutas
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paBuscarRutas`(IN parada VARCHAR(1))
BEGIN 
	SELECT r.origen, r.destino, r.tiempoEstimado, r.codigo FROM Ruta r INNER JOIN Parada p ON p.nombre like '%parada%' INNER JOIN Ruta_Parada rp ON rp.codigoRuta = r.codigo AND rp.codigoParada = p.codigo;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paBuscarUsuario
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paBuscarUsuario`(IN nomb VARCHAR(100), IN puest VARCHAR(30))
BEGIN 
	SELECT * FROM Usuario WHERE nombre like CONCAT('%',nomb,'%') AND rol like puest;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paEliminarImagen
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE PROCEDURE `paEliminarImagen`(IN numero INT)
BEGIN 
	SELECT * FROM imagen WHERE num = numero;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paEliminarUsuario
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paEliminarUsuario`(IN contra VARCHAR(30), IN usu VARCHAR(20))
BEGIN 
	DELETE FROM Usuario WHERE clave=contra AND cedula=usu;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paInsertarEncomiendas
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paInsertarEncomiendas`(IN usr_e int, IN usr_r int, in suc_s int,in suc_d int, in c_rem varchar(9), in n_rem varchar(255),in t_rem varchar(8), in c_dest varchar(9), in n_dest varchar(255),in t_dest varchar(8))
BEGIN 
	insert into Encomienda(
		usuario_emisor, 
        usuario_receptor,
        sucursal_salida,
        sucursal_destino,
        cedula_remitente,
        nombre_remitente,
        telefono_remitente,
        cedula_destinatario,
        nombre_destinatario,
        telefono_destinatario,
        fecha_enviado
	)
    values(usr_e, usr_r , suc_s , suc_d, c_rem ,n_rem ,t_rem , c_dest, n_dest ,t_dest,NOW());
    select LAST_INSERT_ID();
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paInsertarImagen
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paInsertarImagen`(IN nomb VARCHAR(50), IN numero INT)
BEGIN 
	UPDATE imagen SET nombre = nomb WHERE num = numero;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paInsertarTiquete
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paInsertarTiquete`(IN codRuta INT, IN precio INT, IN estado VARCHAR(20), IN codSucursal INT)
BEGIN 
	INSERT INTO Tiquete(codigoRuta, costo, estado, codigoSucursal, fecha)VALUES (codRuta,precio,estado, codSucursal,(SELECT CURDATE()));
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paInsertarUsuario
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paInsertarUsuario`(IN contra VARCHAR(30), IN usu VARCHAR(20), IN nomb VARCHAR(100), IN puest VARCHAR(30))
BEGIN 
	INSERT INTO Usuario VALUES (usu, nomb, contra, puest);
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paModificarUsuario
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paModificarUsuario`(IN contra VARCHAR(30), IN usu VARCHAR(20), IN nomb VARCHAR(100), IN puest VARCHAR(30))
BEGIN 
	UPDATE Usuario SET nombre=nomb, rol = puest WHERE clave=contra AND cedula=usu;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paMostrarBus
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paMostrarBus`(IN capacity int)
BEGIN 
	SELECT * FROM Bus where capacidad >= capacity and tipo != 'Publico';
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paMostrarHorario
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paMostrarHorario`(IN codRuta INT)
BEGIN 
	SELECT * FROM HoraSalida WHERE codigoRuta = codRuta AND hora BETWEEN (select curTime()) AND '23:59:59';
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paMostrarImagen
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paMostrarImagen`()
BEGIN 
	SELECT * FROM imagen;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paMostrarPaquetes
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paMostrarPaquetes`(IN num INT)
BEGIN 
	SELECT * FROM Paquete WHERE numeroGuia = num;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paMostrarParadas
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paMostrarParadas`(IN codRuta INT)
BEGIN 
	SELECT p.*, rp.precio FROM Parada p INNER JOIN Ruta_Parada rp ON rp.codigoRuta = codRuta AND rp.codigoParada = p.codigo;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paMostrarRutas
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paMostrarRutas`()
BEGIN 
	SELECT * FROM Ruta;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paMostrarUsuarios
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paMostrarUsuarios`()
BEGIN 
	SELECT * FROM Usuario;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.paVerificarUsuario
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `paVerificarUsuario`(IN contra VARCHAR(10), IN usu VARCHAR(20))
BEGIN 
	SELECT * FROM Usuario WHERE clave = contra AND cedula = usu;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.spCargarRutas
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `spCargarRutas`()
BEGIN
	select * from testRutas;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.spCargarSucursales
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `spCargarSucursales`()
begin
	select * from Sucursal;
end$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.spComentarioEncomienda
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `spComentarioEncomienda`(in cod int, in c varchar(255))
BEGIN
	update Encomienda
    set comentario=c
    where id=cod;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.spInsertarPaquetesEncomienda
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `spInsertarPaquetesEncomienda`(in en int, in dim int, in cat int, in peso double, in des varchar(255))
begin
	insert into Paquete(dimensiones,categoria,peso,descripcion)
    values(dim,cat,peso,des);
    insert into PaqueteEncomienda values(en,(select LAST_INSERT_ID()));
end$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.spRecibirEncomienda
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `spRecibirEncomienda`(in cod int,in fecha date,in usr int)
BEGIN
	update Encomienda 
    set fecha_recibido=fecha, usuario_receptor=usr
    where id=cod;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.spTestBuscarRuta
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `spTestBuscarRuta`(in tipo int, in busqueda varchar(255))
begin
	if tipo=0 then
		select * from testRutas where salida like busqueda;
	elseif tipo=1 then
		select * from testRutas where destino like busqueda;
    end if;
end$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.spTestParadasRuta
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `spTestParadasRuta`(in ruta int)
BEGIN
	select p.id, p.nombre, p.latitud, p.longitud
    from testParadas p, testParadasRutas pr
    where p.id=pr.idParada and pr.idRuta=ruta;
END$$

DELIMITER ;

-- ----------------------------------------------------------------------------
-- Routine ucrgrupo3.spTestRutaHoras
-- ----------------------------------------------------------------------------
DELIMITER $$

DELIMITER $$
USE `ucrgrupo3`$$
CREATE  PROCEDURE `spTestRutaHoras`(in horario int)
begin	
	select DATE_FORMAT(r_hrs.hora,'%k:%i %p') as `hora`
	from testRutaHoras r_hrs 
	inner join testRutasHorarios r_h
	on r_h.id=r_hrs.idHorario and r_hrs.idHorario=horario;
end$$

DELIMITER ;
SET FOREIGN_KEY_CHECKS = 1;
