
-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: 68.178.217.14
-- Generation Time: Feb 01, 2017 at 11:02 PM
-- Server version: 5.5.43
-- PHP Version: 5.1.6

SET FOREIGN_KEY_CHECKS = 0;

--
-- Database: 'ucrgrupo3'
--

--
-- Dumping data for table 'alquiler_bus'
--


--
-- Dumping data for table 'Bus'
--

INSERT INTO Bus VALUES('12346789', 35, 'Bueno', 'Privado');

--
-- Dumping data for table 'CategoriaPaquete'
--


--
-- Dumping data for table 'DimensionesPaquete'
--


--
-- Dumping data for table 'Encomienda'
--


--
-- Dumping data for table 'imagen'
--


--
-- Dumping data for table 'Paquete'
--

INSERT INTO Paquete VALUES(1, NULL, NULL, 'dasdasdsa', 21);
INSERT INTO Paquete VALUES(2, 0, 0, 'dasdasdsa', 21);

--
-- Dumping data for table 'PaqueteEncomienda'
--


--
-- Dumping data for table 'Sucursal'
--


--
-- Dumping data for table 'testParadas'
--

INSERT INTO testParadas VALUES(1, 'Parada 1', 10.202636, -83.795971, NULL);
INSERT INTO testParadas VALUES(2, 'Parada 2', 10.210091, -83.795958, NULL);
INSERT INTO testParadas VALUES(3, 'Parada 3', 10.212983, -83.796003, NULL);
INSERT INTO testParadas VALUES(4, 'Parada 4', 10.213831, -83.790225, NULL);
INSERT INTO testParadas VALUES(5, 'Jimenez', 10.21083, -83.74471, 2);
INSERT INTO testParadas VALUES(6, 'Guapiles', 10.21667, -87.78333, 1);

--
-- Dumping data for table 'testParadasRutas'
--

INSERT INTO testParadasRutas VALUES(1, 1, '00:00:30', NULL);
INSERT INTO testParadasRutas VALUES(1, 2, '00:00:30', NULL);
INSERT INTO testParadasRutas VALUES(1, 3, '00:00:30', NULL);
INSERT INTO testParadasRutas VALUES(1, 4, '00:00:20', NULL);
INSERT INTO testParadasRutas VALUES(2, 5, '00:35:00', NULL);
INSERT INTO testParadasRutas VALUES(3, 6, '30:00:00', NULL);

--
-- Dumping data for table 'testRutaHoras'
--

INSERT INTO testRutaHoras VALUES(1, 1, '07:30:00');
INSERT INTO testRutaHoras VALUES(2, 1, '08:00:00');
INSERT INTO testRutaHoras VALUES(3, 1, '08:30:00');
INSERT INTO testRutaHoras VALUES(4, 1, '09:00:00');
INSERT INTO testRutaHoras VALUES(5, 3, '07:00:00');
INSERT INTO testRutaHoras VALUES(6, 3, '08:00:00');
INSERT INTO testRutaHoras VALUES(7, 3, '09:00:00');
INSERT INTO testRutaHoras VALUES(8, 3, '10:00:00');
INSERT INTO testRutaHoras VALUES(9, 2, '01:00:00');
INSERT INTO testRutaHoras VALUES(10, 2, '02:00:00');

--
-- Dumping data for table 'testRutas'
--

INSERT INTO testRutas VALUES(1, 'Guapiles', 'Guacimo', 'Ninguno', NULL);
INSERT INTO testRutas VALUES(2, 'Guapiles', 'Siquirres', NULL, NULL);
INSERT INTO testRutas VALUES(3, 'Siquirres', 'Guapiles', NULL, NULL);

--
-- Dumping data for table 'testRutasHorarios'
--

INSERT INTO testRutasHorarios VALUES(1, 1, 'Lunes', 'Viernes');
INSERT INTO testRutasHorarios VALUES(2, 1, 'Domingo', NULL);
INSERT INTO testRutasHorarios VALUES(3, 3, 'Lunes', 'Viernes');

--
-- Dumping data for table 'Tiquete'
--

INSERT INTO Tiquete VALUES(1, 1, 1, 265, '2017-01-25', '1');
INSERT INTO Tiquete VALUES(2, 1, 1, 265, '2017-01-28', '1');

--
-- Dumping data for table 'Usuario'
--

INSERT INTO Usuario VALUES('123456789', 'Nombre', '1234', 'ventanilla');
INSERT INTO Usuario VALUES('702220782', 'Marlon Castro', 'pass123', 'administrador');

SET FOREIGN_KEY_CHECKS = 1;
