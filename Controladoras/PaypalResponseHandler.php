<?php
require 'PaypalIPN.php';
require '../Datos/Conexion.php';
require '../libs/phpqrcode/qrlib.php';
require '../libs/phpqrcode/qrconfig.php';
require 'Mailer.php';


$ipn = new PaypalIPN();

// Use the sandbox endpoint during testing.
$ipn->useSandbox();

//verificar la respues del IPN
$verified = $ipn->verifyIPN();

if ($verified) {

    $payment_status = $_POST['payment_status'];//read the payment details and the account holder

    if($payment_status == 'Completed'){

        //Conectar a la base de datos e insertar los datos de la compra
        $con = conectar();
        if(!mysqli_query($con,"insert into test values(null,'Success',NOW())")){echo 'error mysql';}
        desconectar($con);

        //Nombrar el archivo con el numero de transaccion
        $filename="../storage/{$_POST['']}.png";

        //generar codigo QR
        //El contenido del codigo es toda la informacion referente a la transaccion y al usuario
        QRcode::png(
            "{$_POST['']};{$_POST['']};{$_POST['']};{$_POST['']};{$_POST['']}",
            $filename,
            'L',4,2
        );

        //Anclar la imagen QR al contenido HTML del correo
        getMailObject()->AddEmbeddedImage($filename, 'qrcode');

        //Enviar email al comprador
        sendEmail("Verificación de compra exitosa",'<img src="cid:qrcode">',$_POST['payer_email'],true);
    }
}
else{
    // manejar el fallo de la transaccion
}

// Responder con estado 200, para indicar al IPN que se recibió respuesta
header("HTTP/1.1 200 OK");
