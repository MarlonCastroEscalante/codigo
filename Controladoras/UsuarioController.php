<?php
session_start();

if(isset($_GET['handler'])){
    switch ($_GET['handler']) {
        case 'logout': logout(); break;
        case 'redirect': redirectUser($_SESSION['user']['rol']); break;
    }
}

function logout(){
    unset($_SESSION['user']);
    header('Location: ../index.php');
}

function redirectUser($user){
    switch ($user) {
        case 'administrador': header('Location: ../Vistas/administrador.php'); break;
        case 'encomienda': header('Location: ../Vistas/encomiendas.php'); break;
        case 'ventanilla': header('Location: ../Vistas/ventanilla.php'); break;
    }
}

function redirect($current){
    if (isset($_SESSION['user'])) {
        if (($current!=='p') && (($_SESSION['user']['rol']==='ventanilla' && $current==='e') || ($_SESSION['user']['rol']==='encomienda' && $current==='v') || ($_SESSION['user']['rol']==='administrador' && $current==='v'))) {
            header('Location: ../Vistas/loginerror.php');
        }
    }
    elseif ($current!=='p') {
        header('Location: ../Vistas/loginerror.php');
    }
}

 ?>
